<!--

SPDX-FileCopyrightText: 2020 Riley Duffield <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# An Old Enemy

I scanned my surroundings constantly. Even though the banquet hall was "safe", I felt uneasy. The worst attacks occurred in safe areas.

When a servant offered me wine, I declined it. Alcohol muddled the senses and mind. I missed my weapons. Only a knife hid under my dress. No armor fit under it either.

Though I was an honoured guest, I kept my bangs over the left side of my face. If anyone saw my tattoos, they would throw me out immediately. I resented my brands. They made a normal life impossible.

An unrecognized presence crept behind me. My left hand hovered over my concealed knife. I turned. Drapes concealed the object of my fear.

A man's hand beckoned me.

"If you wish to talk to me, do it in the open," I ordered.

The masked man looked both ways before he emerged. He shoved a bundle into my hand and fled the banquet hall.

To prevent an explosion, I surrounded the bundle with my unstarlet. After ten minutes, I realized that the bundle contained no bomb. The other party attendees shook their heads at my "paranoia".

For once, I appreciated my dress gloves, which protected me from potential toxins in the bundle. With trembling hands, I loosed the bundle. A carbine, my carbine, rested at my feet. The last time I held my rifle, I fought for the survival of my people. I lost that fight.

I retied the bundle and tucked it under my arm. My gaze darted from window to window. My enemies must be near.

*Why did they give me a weapon?* I thought.

A man with two chalices approached me. His mind seemed familiar, but not identifiable.

"I thought you might like a drink, Valora," he said as he proffered a cup.

I smoothed my hair away from my right cheek. People normally left when they saw my brands. No one wanted to talk to a traitor, thief, etc.

The man did not stand down.

"I do not drink," I said.

"I am sorry," he replied.

To cope with his fear, he downed both chalices. As he tilted his head back, I noticed a scar on his neck.

I recognized him, the Potentate. His armies exterminated my people with disease weapons. He personally slaughtered the last of us.

I ripped the cover off the carbine and loaded it. My barrel pointed at his forehead.

"Why are you here?" I asked.

My finger hovered over the trigger.

"I came to make things right."

"How? By killing the last Red person. You tried that already. I am extramortal; you cannot keep me dead."

"I know that. I came to be killed, not to kill," he said.

I glanced around the room for the Potentate's soldiers. None appeared. Party attendees shrank from the confrontation.

"I brought no soldiers. I came because you deserve revenge."

"You want me to kill you?" I asked.

"Yes. Why did you spare me? Why do you haunt my dreams? I can't sleep. I hate myself," he said.

"I spared you because your successor would have been even more violent. I could not bear to release his evil on the world."

"He is Potentate now." the man hung his head.

"What happened after I left?" I asked.

"Your face tormented me. I renounced my throne. My wives divorced me. I found every scrap of paper I could in your homeland. After you kill me, at least you will have some memories from your people."

A red-haired girl with a toddler at her side rushed toward me. An aura of love surrounded her.

"Please don't kill him!" she shouted.

I turned my head without turning the rifle.

"Who are you?" I asked her.

"Please don't kill my husband," she pleaded on her knees. "I need him. Our daughter needs him."

*They cannot be married. She has red hair. She looks similar to my people.* I thought.

"Is she really your wife?" I asked him.

"Yes. Please, Avalynn, go away. I don't want you to see this."

"I know what he did to you, but he has paid for his crimes. His exile is torture. He saved my life. I will die before I let you kill him." Avalynn announced.

"How can you love a red-haired woman? You hated my people," I asked.

"I was wrong. I know her red is different than yours, but I wish for her to become one of your people."

Her hair had hints of orange. Mine was blood-red. She also had brown eyes instead of red.

"You want to restore my people," I asked.

"I know my child doesn't look quite like you, but yes. I want to restore your people," he said.

"I wish the same. I've heard stories of your bravery. I want my child to be like you," Avalynn said.

The emotion of Avalynn's love overtook me. I dropped my magazine and emptied my chamber. Brass rang as the round hit the floor.

"I do not hate you," I embraced him. "Your crimes are forgivable."

"I swear to serve you forever," he said. "I wish I could undo my murders."

He wept on my shoulder. The toddler tugged at my skirt. I tussled her red hair.

"What is her name?" I asked Avalynn.

"Her name is Valora. He named her after you."
