<!--

SPDX-FileCopyrightText: 2020 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# The Millionth Bottle

An executioner is sent to kill a wicked man if he doesn't repent in 14 days. The
evil man's wife jumps in front of the sword.

As a punishment for killing the wrong person, the executioner must serve one
million bottles of wine to the man.

She tries to convince the man to leave the wine cellar. He will be immortal
until he drinks the last drop.

He doesn't listen. He drinks the last drop and the executioner is freed.

She becomes a wish-worthy instead.
