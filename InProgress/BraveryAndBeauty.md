<!--

SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Bravery and Beauty

An agressive colonel sends a gorgeous corporal, Katherine, to allure a kaiju because analysis shows that the kaiju respects beautiful things, while leaving other things in a wake of destruction.

The kaiju is telepathic and emerged after an earthquake woke it from its hibernation.

When the kaiju senses that Katherine's affection is disingenous, it rejects her and untethers their minds.

The kaiju chooses a plain, but kind woman, named Mary, as its mouthpiece. The woman's brother is a soldier, responding to the incident.

The kaiju is lured to a trap and killed with explosives. The brother died while trying to warn the kaiju. The body of its mouthpiece dies when the kaiju does.

Katherine gets a headache, then realizes that Mary was placed inside her mind to save Mary's life. Together, they seek another kaiju, so Mary can be reborn as a kaiju. 