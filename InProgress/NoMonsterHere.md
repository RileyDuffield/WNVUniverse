<!--

SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# No Monster Here

1. A poor couple wins a week stay at a remote hotel for their honeymoon.
2. Once there, they find out that the man is not human and that a ring on his right hand has kept him disguised.
3. The people who gave them the ticket for the stay want to kill the "monster".
4. The wife convinces them to keep him alive.
5. The cost is that the couple must stay locked in a cell for three weeks with only plants to eat.
6. They expect the monater to eat her because he needs meat to survive.
7. The monster enters a coma after two and a half weeks.
