<!--
SPDX-FileCopyrightText: 2021 Seth Patterson

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Triple Dragon

While Crystal is learning how to control her wish-granting ability, she meets a group of three teenagers, a brother and sister, and a friend. They wish to be able to transform into "a dragon". Because the stars forced Crystal to interpret wishes literally, she made the teenagers able to transform from three humans into a single dragon and back again.

