<!--

SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Fixer Meets Ella

The marines touched down in the only airfield in the Twilight Queendom. They saluted me smartly as they disembarked, I returned their salute and put them at ease.

"Welcome, Apex Squad," I said. "Congratulations on your latest mission. The Blwkw Empire is grateful for your assistance and has started diplomatic talks with us."

"Our pleasure, Twilight," Captain Pine, an evergreen dryad, said. "We heard you need us here for extra security."

"Yes," I replied. "Natia's forces are pushing closer to our border. You will augment my bodyguards."

"Yes, Ma'am," Pine said. "It's an honor."

"Each of your men will be assigned to a member of the royal family," I said. "Greet your wives, then meet me at the Palace at 19:00 hours." I pointed to Private Fixer and Private Oslo. "You two, come with me."

They slung thwir duffel bags over their shoulders and followed me to the palace. My personal guards, Sergeant Willow and Sergeant Jawbreaker, scanned for threats and kept their swords ready.

"Oslo," I said. "You are assigned to Ella's children. You will attend them at all times and sleep outside their room. They are bright and excelling in their studies. Her sons want to be marines, so expect a lot of questions."

"Yes, Twilight," Oslo said. "I love talking to youngsters."

"That's why I chose you," I smiled. "Fixer, you're assigned to Ella. You and Sergeant Jawbreaker will work together."

"Yes, Twilight," Fixer said. "Permission to ask a question."

"Granted," I said.

"Is she the Ella who formerly ruled the Remnant Lamds and Lovely Wood?" Fixer asked.

"Yes," I said. "She is under house arrest, so she cannot leave the palace. If the palace is lost, you will fight to the death to protect her. There can be no retreat."

Fixer shuddered.

"I will not force you to take this assignment," I said. "You would not be alone if that happened."

"I will do my duty, SilkEba," he said.

"Thank you, Fixer," I said.

We passed gate guards and entered a black, stone building. Tapestries in greens and purples decorated the walls, portraying the mighty warriors and healers of old.

Oslo met his charges, who peppered him with questions about his armor and weapons. Outside my bower, I showed Fixer to his room, a closet with a cot and shelf.

"I am sorry that it is cramped," I said.

"It is like my bunk in our freighter," he said as he swung his duffel onto the cot. "I'm honored to be here. Queen Nike sends her greetings."

"I'm honored to have a Nikean marine here," I smiled.

Ella slithered into the hallway and Fixer's jaw dropped. Ella bowed to me and I scratched her jaw.

"This is your new guard, Private Fixer," I said. "He's a Nikean member of our coalition."

Fixer stared.

"I am sorry if I startle you," Ella hissed. "I was cursed for my arrogance, so I bear the body of a snake."

"It will be an honor to guard one so," Fixer paused. "So beautiful. Your eyes are," he stumbled again. "mesmerizing."

"Is he always like this?" Ella asked.

"He has a history of being distracted by women," I replied. "This time, it's not his fault. I felt a burst of my power, I'm sorry."

Fixer shook himself. "SilkEba, what do you mean?"

"When Natia and Ella became snakes, Natia's power transferred to me. I still don't know how to control it," I said.

"Her power makes people fall in love," Ella explained.

I touched Fixer's forehead and removed the crush I had accidentally caused. "You are free now," I smiled. "I chose you for this role because I knew you would see the person inside when you met Ella. Most people only see her past crimes."

"I will fulfil my duty to her, SilkEba," Fixer saluted. I returned his salute and left.

***

"What do you think of our palace?"Jawbreaker asked in the garden as Ella sunned herself on a rock.

"The food is good and the people here don't hate me," Fixer said. "Where I came from, being a silkwing was a crime."

Jawbreaker said nothing. She could not fathom how a creature, blessed by the Twilight, could be hated.

"Do you think the Ildylian army will reach the Twilight Queendom?" Fixer asked.

"I don't know," Jawbreaker said as she scanned the rooftop. "Empress Enna has  refused our aid, so we must hope that the Remnant Lands can hold them off. They are outgunned and cannot fight at night like the Ildyllians."

"We should be out there, driving back Natia's forces," Fixer said. "Not cooped up waiting for an attack."

"Some of us have no choice," Ella lifted her head. " If the fight comes here, I will die. Natia wants my head."

"Forgive me," Fixer said. "I forgot."

"It is alright, private," Ella said. "I deserve what is coming. I have much blood on my hands, or, well you know what I mean."

Ella raised herself to her full height and looked Fixer in the eyes. "Promise that you will get my children to safety if the palace falls."

"Lady Ella," Fixer said. "Private Oslo will do that. My orders are to fight to the death to protect you. I would even give my venom to save you."

"I understand," Ella said. "I would keep that sentiment to yourself."

The trio left the garden and visited the nursery. Ella's Remnant-Dryad children greeted her. Despite her snake form, they only saw a loving mother. My daughter, Kin, saw a best friend. She relished their hugs and quizzed them on their lessons.

***

That night, Fixer slept in his bunk (only female guards were allowed in my bower at night). Ella sat on a stool as I stroked her smooth jaw.

"What do you think of Fixer? she asked.

"He is a loyal ally," I said. "In the past, he was a flirt, but he grew out of that."

"Today, he offered to bite me to save my life," Ella said.

"He would probably offer that to any single woman," I said. "He was raised to believe that silkwings exist to serve and bless all other people."

"His bite would make us married," Ella said. "Are you sure he is not in love with me?"

"I removed the love that I caused him to feel," I said. "If he feels anything, it is his own choice."

Ella drooped. "How?" she asked. "My scales tell everyone that I am a murderer, witch, and immoral woman. I'm not beautiful. How could someone like me?"

I cupped her head in my hands and looked into her gorgeous eyes. "You were those things," I said. "You are washed clean by our Savior. Justice is being met by this house arrest. You don't need to feel shame anymore."

"Fixer deserves better," Ella said. "I don't even look like a woman. I'm not human."

"Ella," I said. "When you find love again, my power will give you what you need to have children. What if thst will be God's way of restoring your humanity?"

"Your power has already failed," Ella said. "Your eyes are supposed to make people lose their snakeness. I stare at your eyes every day."

"I do not know why it does not work," I said. "I pray every day for you to be restored. What if a silkwing's bite heals you?"

"What if it curses the silkwing with being a snake too?" Ella said. "I miss what I had with Tearborn, but I cannot risk hurting someone else with my broken love." She slithered into her closet.

I laid on my bed, kept awake by cares and stolen woes. Exile kissed me in my mind. I longed for my husband to be physical, rather than trapped in my mind.

***

***

Ella awoke, buried under rubble. Armor wrapped around her and two antennae protruded from her head.

*'What happened?'* she wondered.

*'I bit you,'* Fixer thought. *'Natia shot you and you were going to die.'*

*'Thank you,'* Ella said. *'I hope you're not a snake now.'*

*'We'll find out,'* Fixer said. *'First, we need to get out of this rubble.'*

As they slithered to the surface, Fixer's armor protected them. Twilight soldiers, attracted by the noise, approached with raised crossbows.

"Who are you?" they asked.

"Elixer," Fixer quipped. "We're a Nikean marine and a prisoner under house arrest."

Fixer peeled back his armor from Ella's face. The soldiers lowered their weapons.

Fixer explained what had happened as he and Ella picked his armor out of the rubble. The Ildylians had stolen his rifle and pistol. After the soldiers left, Fixer changed into his natural form and donned his armor.

"I'm not a snake," Fixer said. "I got your skills when I bit you. I'm glad you're good at hand-to-hand combat, that means I am too now."

"I'm sorry you had to do that," Ella said. "You didn't have to save me."

"I didn't just do it to save you," Fixer said. "I would have married you after the war. This just made it happen sooner."

Fixer held her cheeks and kissed her scaly lips. She wrapped around his armor in her approximation of a hug.

"You're beautiful," Fixer said. "Your voice and eyes are so alluring."

Ella smiled.

***

In the surviving part of the palace, Fixer and Ella joined the rest of the soldiers and marines. Most had wounds, which their dryad brothers, worked to heal with their leaves.

"We expect them to return," Major Smoothbark said to his men. "We evacuated the Twilight and her family, but we must ensure that this city doesn't fall. We're cut in half, so our priority is rebuilding communication with the Western side."

The Major assigned men to their tasks, but they felt hopeless. Every Ildylian had a gun, but few Twilight soldiers did.

"Maybe the witch can help," a dryad marine said. "What's your best spell?"

"My wife is not a witch," Fixer shouted and took a step toward the dryad.

"Easy, son," the Major held up his hand.

Because Ella would be safest when armored by Fixer, Major Smoothbark ordered Fixer to give his armor to the dryad and armor his wife. Fixer shapeshiftwd to look like marine armor and give Ella arms and legs.

Elixer, a combination of Ella and Fixer's names, aided the medical staff.

*'I'm sorry for the shame I bring you,'* Ella thought.

*'The SilkEba said you gave up your evil ways,'* Fixer thought. *'I know she's right, so I have no regrets for my choice.'*

*'Thank you, Fixer,'* she said. *'You've restored me. You have no idea how good it feels to walk upright or have my scales covered.'*

Elixer worked into the night as a seamless team. Ella allowed Fixer to control their movement, unlike other people who fight their armor at first. They comforted the dying and changed bandages.

*'I love you,'* Ella thought.*'This is one of the most wonderful things to happen to me since I gained my other silk friends.'*

*'Who are they?'* Fixer asked.

*'Crystal, Shadow, and Ruby,'* Ella thought. *'They're triplets of a sort.'*

Wounded streamed into the building all the next day. Fixer longed to be part of the fight, but knew his duty to protect Ella. Finally, the captain in charge of the hospital allowed them to rest.

Deep sleep enveloped them as soon as their head touched their pillow. Ella had never felt safer.

***

Five hours later, they awoke to the news that Natia's forces were driven out of the city. Enna had sent reinforcements and Twilight soldiers had captured many rifles.

Ildylian prisoners cowered inside makeshift prisons, wondering when they would be eaten. In a previous war against Ella, her men had treated Ildylians terribly, even stooping to cannibalism. Though Ella had not ordered their actions, the warcrime tribumal held her responsible.

A spider emerged from the corner and Elixer prepared to strike. Then they noticed the spider's leafy form.

"I'm one of you," the beautiful dryad-spider said. "Please don't hurt me."

"Sorry," Elixer told the nurse. "We're a little jumpy. Stay with us. We'll make sure no one hurts you."

"Thank you," she said.

Less sleepy soldiers responded more positively to the nurse. Her eight legs were made of vines and her green skin drank in light.

"You look different from other spiders," Fixer said.

## Alternative

Natia burned Ella's side with starfire. "Which of you silkwings will bite jer to save her life?" Natia hissed. "She already has a silk-friend."

Ella's scales terrified the silkwing prisoners. They announced Ella as a murderer, seductress, liar, and witch. Though the silkwings were kind-hearted, they could not bring themselves to wed Ela to save her.

Blood pooled below Ella as she writhed and gasped.

"No one loves you," Natia announced. "The Woethief and yourfamily left to save their own skins. You are vileand unloveable."

Ella sobbed.

Fixer crawled into the room. Natia's soldiers dragged him to his feet and pinned his arms behind him.

"I will save you, Ella," he gasped. Hope filled Ella's eyes.

"You just want to save yourself," Natia accused. "No matter, you can bite someone else to be healed. Bring Itke here."

A lovely star dazzled the room. She sized up Fixer and liked what she saw.

"You and Itke will become powerful together," Natia said. "I know about you, Fixer. Join me and wealth and power will be yours."

Itke made her form as attractive as possible. Her gentle smile and delicate features invited Fixer to bite her. She held out her wrist in anticipation.

As the soldiers released Fixer, he lunged for Ella. Soldiers dragged on his feet, but he siezed Ella by the tail and buried his fangs in her.

His body morphed into armor for the dying woman and healing filled both of them.

"The Silk-Eba loves us," Ella and Fixer said in unison.

"No, just this foolish marine does," Natia said.

"Itke, give them your starlet," Natia ordered."If they try to separate, kill them with it."

"Yes, your worshipfulness," Itke obeyed. Starfire formed a noose around Ella and Fixer's neck.

"Take the prisoners away," Natia ordered. "Send Ella and Fixer to the predetermined place. Kill any Remnant that you find."

Fixer wanted to fight, but his orders were to keep Ella alive. At gunpoint, they walked to the entrance of the palace.

"Please do not make me leave," Ella said. "I will be killed if I break house arrest."

"Private Fixer is your home now," Itke said. "You may not leave his armor for the remainder of your sentence."

"That's not fair to him," Ella hissed. "He didn't want to bite me. He only did it to follow orders."

"I am taking pity on you," Itke whispered. "Natia does not know what she asked. Now that you have my starlet, we are friends. Its fire cannot harm you."

"Thank you," Ella said. Her head drooped.

Soldiers shackled their hands and feet, then put them in the back of a cart. The city's stone buildings melted under starfire and trees blazed as collateral damage.

"Thank you for saving me," Ella said. "I deserved to die. My scales don't lie."

"No one needs to see your scales anymore," Fixer said. "My armor has erased your past. I'm not sorry that I bit you, though I wish it were under better circumstances."

"But, I'm a monster," Ella said.

"Were a monster," Fixer replied. "The Silk-Eba trusts you, so I do too."

"How?" Ella asked.

"I've seen my true self, during dark-jump," Fixer said. "I saw a womanizing, angry, selfish child. It broke me."

"Twilight said you changed," Ella said.

"You changed too," Fixer replied. "I really am happy to love you."

"You aren't allowed to leave for 19 years," Ella wept. "My crimes have imprisoned you."

"You are lovely," Fixer said.

"But I turned you into a snake," Ella wailed.

"No," Fixer said. "I bit you and gained your skills, not your physical traits."

Ella breathed a sigh of relief.

"Hold me," Ella asked. "I'm so tired."

Fixer held Ella as he protected her. The warmth of the starlet and the exertions of their day lulled both of them to sleep.

***
