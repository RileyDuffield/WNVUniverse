<!--

SPDX-FileCopyrightText: 2023 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Kin's Debut

1. Kin was disowned, so she has no one to introduce her as a debutante.
2. Kin asks WNV to introduce her.
3. The stars are angry that a twilight person has visited them.
4. Kin announces that she found the Leader of Light (WNV).
5. A star disbelieves Kin and challenges her to a duel.
6. The star illuminates half of the sky.
7. With WNV's empowerment, Kin illuminates the whole sky so brightly that stars cast shadows.
8. Kin's new brightness goes to her head. She continues to vie for the stars' approval instead of submitting to WNV.
9. When Kin becomes a nebula, WNV becomes her real mother.
