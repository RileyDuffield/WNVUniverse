<!--

SPDX-FileCopyrightText: 2021 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Kin's Redemption

Before the Darkish invasion of Ildylia, Kin developed a case of nebulizing. Since she did not have anywhere else to turn, she asked WNV to heal her. WNV tried her hardest to heal her but failed.

After Kin became a nebula, she threw the pieces of WNV's statue that she had collected into the void because she was mad at WNV. The stars banished her and stole her property.

She wandered until she turned up near WNV. WNV embraced her and offered forgiveness.

As a parting gift, Kin gave her only nebula orb to WNV.

Kin found a peddler who promised to give her a terrestrial body with many sensory organs, eyes, and the ability to breathe underwater. She traded all her colors, except red, for the potion to make her a hawk-griffin.

Without her consent, her essence was added to the potion. The stars delivered the potion to WNV with instructions telling her that if she drank it her tattoos would disappear. WNV drank it and Kin took on a physical form similar to WNV's, except that it was red instead of gray and white.

Or WNV is the surrogate mother of Kin when she is reborn.

Kin spent years searching for all the statue fragments until she finally completed WNV. WNV's family members were unable to recognize her until the statue was completed.

After the Darkish invasion of Ildylia, either Kin or Gemma cared for the Darkish-spider that WNV adopted.
