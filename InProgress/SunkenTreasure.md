<!--

SPDX-FileCopyrightText: 2023 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Sunken Treasure

1. A team of explorers wants to find the lost civilization of King Lu.
2. The team visits an abandoned submarine base, while another member
   investigates a royal seal of King Lu's face at a library of rubber stamps,
   designed to document the kingdom's symbols.
3. The team, five divers, prepares to dive, but a team member decides to leave
   his dagger behind. They did not tell anyone where they were going.
4. The team dives down a flooded stairwell and notices that the seal of King
   Lu's face matches a portrait on the wall.
5. Because they think they are close to finding what they are looking for, the
   team leader decides to investigate a while longer, risking running out of
   oxygen in their tanks.
6. The lowest diver, a young woman, is grabbed by a giant squid's tentacles,
   which are disguised with false seaweed.
7. The woman uses a small knife to stab tentacles, but three other team
   members surface, without realizing what is happening.
8. The team leader tries to free the woman from the squid, but cannot.
9. The leader's equipment snags on a piece of twisted metal. He suffocates on
   the way up.
10. The other three divers don't know what killed their friends.
11. The squid eats the woman's brain to copy her memories. It writes messages,
    allegedly from her, on the walls of the stairwell.
12. The squid uses memories, from sailors it ate, to mine the harbor, keeping
    the team from leaving.
13. When the team returns to the stairwell, they see the written messages and
    think their friend is still alive.
