<!--

SPDX-FileCopyrightText: 2023 Seth Patterson <NylaWoethief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Shard Mistress

<!--

* Make natural chapter breaks.
* Share name of land.
* Explain that world is cosmic junkyard and they are salvaging, not mining.
* Are the salvagers slaves, serfs, or prisoners in a penal colony.
* Make Nyla more argumentative.
* What is it like to be in the mind of a sleeping person? Are dreams shared?
* Elaborate on tattooing.
* Is Enna being pregnant redundant?
* Explain twilight again.
* How did Den and Theila show up? Explain that it is a mystery.
* WoeNyl should be relieved that Crystal is safe.
* Clarify why WN does not talk to Enna's child.
* Should WoeNyl try to share her new faith, especially forgiveness, with Enna?
* Clarify who has plant power.
* Describe surface and WN's reaction to it.
* Elaborate on stewards.
* Explain term hostess.
* Does Ella's earlier behavior make sense?
* Why doesn't the mink have a soul?
* In the Woethief, the theme is reflected in the ending. There is no clear theme here. Make it clear that this is about unforgiveness.
-->

## 0 Prologue

We froze into solid silver. The bell, which should have proven our innocence did
not. Someone had removed the clapper, so the bell could not ring to thaw the
statues around us.

Despite our failure to free our step-father and brother-in-law, our sister,
Silver Nectarfang, was free. We took her punishment so her children could be
born in peace, as the grandchildren of Queen Syreeta.

Our two minds, Nyla and Woethief, pondered our choice. Should we have sacrificed
ourself for Silver? We knew the answer. We had done the right thing. Our
connection to our sister was too weak for telepathic speech, but we felt that
she could access our power. We prayed she would use it for good.

In the night, the brightest light we had ever seen filled our vision. We wished
we could shield our eyes, but our frozen limbs stayed put.

"Greetings, Twilight Order," a glowing woman said. Her dress ended in a tail.
She bore no legs, but hovered above the stone ground. "I am Kin. I was sent to
deal with you."

We could not answer her.

"Centin's not the only one who dislikes you," we bemoaned the mention of our
evil, ex-husband. "We stars cannot let your power grow out of control. I will
scatter you to many worlds. If you can find all of your shards, you will win. If
I find your shards first, you will never be restored to wholeness."

She placed her hands on our face. At her touch, our silver body melted. Tears of
molten metal ran down our face. Our twilight fog tried to combat the heat, but
lost. Our clothes burst into flames.

After ten minutes, our body was a shapeless blob of melted silver. Despite Kin's
attack, we stayed conscious enough to feel her cast us into a void. Our body
separated into cooling shards or drops of metal.

## 1 Landing

We fell through emptiness, always numb. Woethief, our compassionate mind
consoled Nyla as we journeyed. Because of our two minds, we were never alone.
Never absent was the shame and pain that Woethief had stolen from those of our
former world.

We missed our daughter, Crystal Comfort, and sister, Theila Sombermirth. Our
minds were once intertwined, but now our loves were lost to us.

Hope pierced our soul like Theila's twilight.

*'Our God is with us,'* Woethief thought despite her tormenting memories. *'We
are not alone. I love you, Nyla.'*

*'I love you too,'* Nyla thought. *'Wherever we are going, we will be together.
I feel Silver pulling from my power, so at least we know she is alive.'*

We smiled inwardly. Though our sister hurt as deeply, we loved her. The thought
of her becoming the Princess of Ildylia thrilled us.

***

*Crash!* We struck rock in a foreign world.

After hours of waiting, a woman picked up our shard. Woethief sensed her
emotions as she delighted in our shiny exterior. Woethief called to her. The
woman slipped us into a fold of her garment. Woethief called again.

*'What is it?'* the woman thought as she held us to her eye.

*'We are a shard of silver,'* Woethief felt stupid for saying the obvious.

*'How are you speaking to me?'* the woman asked.

*'I am speaking in your mind,'* Woethief replied.

The woman took us out and looked at us: disgusted.

*'I told myself I'd never crack,'* she thought.

*'You have not,'* Woethief replied. *'We are real. We can help you.'*

*'How could you help me? Nobody wants to help me!'* Woethief felt the despair
and loneliness in the woman's soul.

*'We are silver and twilight. Our names are Nyla and Woethief,'* Woethief
thought.

*'What can you do for me?'* the woman asked.

We sensed her filthiness, her shame, the decay of her body, and her slavery. We
sensed an opening in the woman's mind that we would fit inside.

*'We can keep you company and take some of your pain from you,'* Woethief
offered. *'We can...'*

*'What is your price?'* she interrupted.

*'We want to live inside of your mind. Please, if you will not take us, then
give us to someone who will,'* Woethief pleaded.

*'You will not force yourself upon me?'* the woman asked.

*'No,'* Woethief replied. *'We know the pain of mental coercion. We wish it on
no one.'*

*'If I decide to... If I decide you are hurting me, you will leave?'* the woman
asked.

*'Yes,'* Woethief replied.

At Woethief's direction, she placed our shard against her breastbone. We fused
to her skin.

***

Senses flooded into us. We remembered how it felt to be alive. Everything our
hostess experienced also affected us.

Our hostess bore a human body. Her pitch-black skin was a dull gray from the
dust that covered everything. Hunger gnawed at her stomach. Soreness permeated
her bruised feet and hands. Shivering racked her frame.

*'I feel no different,'* our hostess thought.

*'Give our power time to work. We can fix the cold now,'* Woethief said.

Our twilight filled her and kept cold from creeping into her bones.

*'Thank you,'* she thought. *'My name is Enna.'*

*'You are welcome,'* Nyla replied. Now that she was physically tied to Enna,
Nyla could speak to her mind too.

Enna stumbled through rubble, searching for something. As soon as we wondered,
we knew what she wanted.

The Winds from Elsewhere blew debris from other worlds into theirs. Enna's
people scavenged for anything useful to enrich their taskmasters.

Dust filled the air like fog. It stung Enna's eyes and pricked her lungs in
ten-thousand places. Exhaustion sliced at her like a knife against a fraying
rope, but she kept on.

Hours of fruitless searching droned on. When the taskmasters decided for
everyone to quit foraging, the slaves presented their finds. Enna had nothing.
Guards beat her, but she did not protest. She received no dinner.

***

At her cell, one-hundred slaves filed in before the portcullis slammed. She
climbed down a rusted ladder into the lowest cubical, where filth covered the
floor up to her ankles.

A man descended to her with a crust of bread in his hand. She reached for the
crust, but the man stuffed it into his own mouth. He laughed as he climbed the
ladder.

***

While our hostess slept, Silver Nectarfang's voice paraded into our minds.

*'WoeNyl,'* our sister, Nectarfang, thought. *'I miss you and I'm sorry for
everything I did. I hope you can hear me.'*

*'I can hear you,'* Nyla thought.

*'I'm accessing your powers,'* Nectarfang continued. *'I hope you don't mind,
but you're more powerful than we realized,'* excitement filled her mind. *'Order
comes from you. It's like your twilight can't stand decay and undoes it.'*

*'Nectarfang,'* Woethief thought. *'Are you safe? Are your children well?'*

Nectarfang did not hear Woethief's thoughts and spoke no more to us that night.

We thanked God that she was alive and prayed that he would work the miracle
Nectarfang called "order" through us. As we concentrated, we felt the chaos of
our hostess's grimy skin tamed by our order.

Kin had called us, "Twilight Order". Who was she and how did she know of our
power before we did?

***

In the morning, our hostess woke with bleeding lips and an empty stomach.
Through our miracle of order, her skin and hair were clean. We wished we could
do more for her.

Enna had a reputation for weakness. Everyone hated her, but they hated everyone
else too. She stumbled through her day, trying to avoid notice.

Sorrow overwhelmed her, as she toiled in vain. She overturned a cart full of
rusted metal. A scourge snapped down on her back, tearing her slip. Welts
decorated her back as the taskmaster struck again and again.

Enna could not stand despite the taskmaster's kicks. While he pummeled her,
Woethief stole Enna's physical pain, freeing her from torment.

Enna clutched a stone in her hand and threw it into her assailant's forehead.
His spirit left his body. Other taskmasters backed away from her, sneering.

*'I've finally done it. I've elevated myself,'* pride swept through her.

*'Why do they not strike you?'* Nyla asked.

*'They only strike those who don't strike back,'* our hostess sassed.

Joy exuded from her contemptuous smirk. She glowered at the guards in loathing.
Everyone resumed their work, except for her.

In return for her kill, she gained the taskmaster's scarce possessions. She
looted his corpse. She now wore a tunic over her slip and hung a whip and
canteen from her belt.

Her newfound charisma enabled her to choose the best area for foraging. Anyone
who neared her tasted her whip.

Then, Woethief tired and our hostess's agony returned. After she collapsed, a
man shoved her to the ground and stole everything except her slip. Her hands
trembled as guilt washed over her.

"I just killed," she wailed. "I told myself I never would."

We said nothing, lest we worsen her contrition.

She scrambled to return to work before the masters beat her again. Shame crushed
down on her like a vice. She felt their stares as she toiled.

By nighttime, she had nothing to turn in to the taskmasters, so they forced her
to continue her search into the next morning.

A silver amulet captured her attention, but another slave wrenched it away from
her. She offered no resistance.

Her timidity remained throughout the day as she avoided others as much as
possible. To our amazement, she never once stopped to rest despite her hunger
and thirst.

In our weakened state, we could do nothing to aid Enna, save whisper encouraging
words into her mind. The encouragement failed. We missed Denrick, our
brother-in-law, he would know what to say.

Despair permeated the air, like the dust, which stung Enna's lungs. Everything
reeked, from her guilt-ridden soul to her infected wounds. Woethief stole her
physical pain again.

***

At feeding time, she snapped out of her misery. Her guilt melted away. With
confidence, she strolled up to a yellow-skinned slave-girl and stole her gruel.

*'Fool, new slaves never guard themselves,'* she guzzled the food and hit the
girl on the head with the bowl. She spat on the girl as she forced her to give
up her brilliantly embroidered blue tunic.

Our hostess's stolen meal did not satisfy her stomach, but took the edge off her
pain.

The man who stole her whip glowered at her.

"You're next," our hostess sneered with murderous intentions and a rock in her
hand.

Woethief stopped capturing her pain and she doubled over.

*'I could have finished him,'* she yelled at our minds.

*'That's why we stopped you. We told you we would help you, not help you to be
evil,'* WoeNyl accused.

*'I'm not evil, this is the way things work around here,'* she justified
herself. *'If you don't kill first, they'll kill you.'*

Despite her agony, she straightened herself.

"I want my..." she trailed off.

Pain dropped her to her knees.

*'Please make it stop,'* she begged.

Woethief tried to steal her pain to no avail. Tears cascaded down her cheeks as
the eyes of the yellow-skinned girl bored into her.

"I'm s...so sorry," Enna sniffled. "I didn't want to do that."

A taskmaster kicked Enna's side and dragged her to her feet by her hair. She
trudged along with the other slaves toward her cell. The wounds on her back
stretched and tore further with each step. Though Woethief tried, she could not
take her woes.

When we reached the cell, Enna chose a top bunk instead of the lowest, which she
occupied previously. The man with the whip strode toward her.

"That's my bunk," he growled. "Get off now."

"Make me," our hostess taunted.

*'Please don't do this,'* Woethief begged.

*'I'll do as I please,'* she retorted.

*'I won't steal your pain so you can murder him,'* Woethief said.

*'Oh, you will,'* she sneered.

Agony flooded into Woethief as our hostess pounced onto the man. He tried to
fight back, but his whip proved useless in such close quarters. Our hostess'
palm strike broke his nose. She picked herself up from the ground with the whip
in her hand.

"Give me the tunic and belt," she demanded.

He growled his defiance. She raked her nails across his face, scratching his left eye.

"Give me the tunic and belt," she repeated.

He obeyed.

<!--Mention power that WoeNyl does not understand. Foreshadow. -->
While she laid on her new, cleaner bed, she forced Woethief to steal all of her
physical pain, but not shame. She felt no remorse for her violence.

*'You will heal my wounds while I sleep and they will not leave scars,'* she
commanded.

*'It will take time. My power is still new to me,'* Nyla thought. Nyla's healing
stemmed from her ability to bring order to chaos. Injuries are chaos.

*'I am sure you will succeed,'* our hostess declared. *'Don't disappoint me.'*

Nyla toiled through the night, but only managed to close a third of the stripes
on Enna's back. Our hostess terrified me.

*'Would another host be just as evil?'* Nyla wondered.

Fatigue threatened to overcome us. We last slept two days earlier. We envied our
hostess as she slumbered without care or dreams.

A faint noise caused Enna to bolt upright. A rat scurried away from her sudden
movement.

*'Why did you let me wake up?'* she asked.

*'We thought you were in danger,'* Woethief replied.

*'It was a rat, stupid,'* our hostess chided. *'You are supposed to listen for
real danger while I sleep. Only wake me if you have to.'*

<!-- Are there vermin like rats in Ildlia? -->
Enna returned to sleep. How could she expect us to recognize sounds in an alien
world? We needed sleep, but if we rested from our vigil, our hostess would harm
us. We did not know how, but she possessed extraordinary mental abilities.

***

In the morning, she felt her back with her fingertips. Disapproval pelted our
soul.

*'You are not living up to my expectations,'* she chastised. *'Why didn't you
heal me?'*

*'I tried. I lacked the time,'* Nyla replied.

*'I don't want your excuses,'* she seethed. *'Try harder.'*

*'I think I can heal you instantly, but it will leave gray scars and requires
water,'* Nyla replied.

*'No. I will not be scarred,'* she announced.

*'We do not need to obey her,'* Nyla thought to Woethief.

As our hostess exited the cell, she passed Joel, the man who had stolen her whip. He
clutched his eye and shrank away from her.

"I am sorry you got hurt," Enna said over her shoulder.

*'Why does she change her attitude so often?'* Nyla wondered.

Everyone stayed away from her during the morning meal. She snatched crumbs from
whoever she wished. No one protested when she took a long draft of water.

Scavenging yielded good results. She found four glass marbles, a broom, and a
boat's anchor. The guards increased her ration by ten percent.

At the night meal, she found everyone she had robbed during breakfast and shared
her food. Her stomach growled as she lay in bed. Guilt filled her mind.

*'I am sorry you found me,'* she thought to WoeNyl.

We were sorry too. She terrified us.

***

The next week, she unearthed dozens of valuables. Despite her prolific hunting,
she coerced other slaves into letting her claim their finds. Her mental energy
overcame their hunger-weakened willpower.

As her wealth grew, so did her cruelty. She stole food with impunity. Her boot
struck anyone who stopped in front of her in the food line. She smacked a cup of
water from a child's hand. The guards noticed.

One morning, the man with the wounded eye did not rise. Infection and fever sent
him into unconsciousness. Enna pitied Joel.

Without the guards' permission, she sneaked back to the cell with food and
water. At the end of the day, she carried Joel up to her bunk. Her frame revealed
the firm muscles that the Remant people are known for.

Her tender care for her enemy baffled us. She worked all day, then spent all
night tending for Joel as well as she could.

With the power of our creativity and order, she healed. Her frame filled out.
The corpse we met a month ago was now a plain, but healthy woman. The gray dust
abandoned her, repelled by our cleanliness, revealing unscarred skin.

The man woke to her flawless smile. Our prayers were answered. Nyla's power had
healed him, at the cost of gray scars. His healed eye looked like the eye Nyla
had borne in our past life.

<!--Have characters grow a relationship instead of instant.-->
After five seconds of deliberation, Joel decided he loved Enna. They worked
fluidly as a scavenging team. Together, they found double what she found on her
own.

After three days, the guards tattooed them. They became married as the white ink
stained their faces. They were couple 36723.

Their first kiss brought joy greater than anything else in their lives. We
feared what would happen at her next mood swing. Enna had acted kind for three
weeks.

With Joe's love, Enna increased in beauty. Her kindness remained.

***

News of a nobleman visiting the great "Discovery Caverns" reached us. The normally
subdued slaves chattered eagerly. Visiting nobles often took worthy slaves out
of the Salvage Caverns to safer jobs.

When Prince Vehemence arrived, all of the slaves lined up as couples. Enna
wished for she and her husband to be chosen together.

Nyla had healed our hostess too thoroughly. Vehemence noticed her when he walked
by. Her shape shouted for attention.

"Who are you?" Vehemence asked as he fingered her cheek.

We prepared our twilight to blind him if he hurt her.

"I am Enna, excellence," she said.

"Your body pleases me," Vehemence replied.

Enna's husband seethed, but dared not strike the prince.

"I challenge your man to a duel," Vehemence said. "I want you for my palace."

Before Vehemence could draw, we wrapped his face with twilight fog. He blindly
struck Joel in the forehead with brass knuckles. Joel didn't get up. When Nyla
tried to heal him, it was too late. Vehemence laughed.

"I will send for you tomorrow," Vehemence walked away, chuckling.

"Of course, my lord," Enna smiled. "It will be my pleasure."

*'How could you say that?'* Nyla asked.

*'I must keep Vehemence happy or I will end up dead like Joel,'* our hostess replied.

Everyone envied her for finding a way out of the Salvage Caverns. She foraged with gusto
because she was excited for her new opportunity.

She shed no tears for her husband.

*'Enna,'* Woethief begged. *'Please release us. We cannot bear your
heartlessness.'*

*'You should have thought of that when you possessed me. You are too valuable to
release,'* she replied.

*'We did it out of kindness,'* we thought. *'We helped you for nothing in
return.'*

*'You needed me,'* she sneered mentally. *'You didn't want to be lonely.'*

***

To our surprise, Enna did not depart from the Salvage Caverns the next day. She gave no
explanation, but went to work as usual.

Tears moistened her cheeks all day. She uncovered few items while digging. At
night-meal, she did not touch her food. The same child she once robbed consumed
her gifted food.

Enna's health deteriorated over the next few weeks. Melancholy clung to her like
the dust of the Salvage Caverns.

*'Enna, you must eat,'* Woethief thought.

*'I can't,'* she thought back. *'Life isn't the same without Joel. I miss him. I
don't care what happens to me anymore.'*

*'But it is not just for you,'* we thought. *'Your child needs nourishment. We
are hungry too.'*

At night-meal, she nibbled half of her dinner. In her bunk, she sobbed herself
to sleep.

***

While she worked, a new guard eyed her. His gray-skinned fist clenched a
truncheon, which he rarely used. Only violent slaves tasted its sting.

Weak slaves, including the yellow-skinned girl, clustered around the guard. In
his presence, safety abounded. A large slave tried to steal a copper scrap from
one of the protected slaves.

The guard pointed at the thief with his truncheon and motioned toward the
victim. The thief drew a shank. As he lunged for the guard, the guard smacked the shank away with a truncheon. With his free hand, the guard seized the thief's wrist
and pressed on the pressure-point. The thief cursed and begged for mercy. The
guard shoved him away in disgust.

A new, female guard entered the cavern during the commotion. Slaves and guards
whistled at her beauty. She scowled and glared.

With the new guards, violence decreased. Their truncheons and keen eyes forced
the wicked to keep their evil below the surface.

Our hostess resolved to befriend the new woman. Men "respected" her because of
her body. They lusted for the guard and hoped to impress her. She was not
impressed and her body remained her own.

She disdained the other guards, especially the gray-skinned one.

Our hostess resolved to test the gray guard's will. At morning-meal, she
"accidentally" spilled some of her gruel on him. He brought her a new bowl.

The next day, she tried to trip him. He righted himself and frowned at her.

*'Why doesn't he get angry?'* our hostess wondered. *'What's wrong with him.'*

*'Maybe he has joy that isn't tied to his circumstances,'* Woethief said.

*'Nobody can have joy in this abyss,'* she replied.

She kept pestering the gray man in small ways. His unflappability exasperated
our hostess.

Weeks passed in drudgery. Enna sought the protection of the guard who called herself, "Sojourner". We doubted that was the guard's real name.

"Warden Sojourner," Enna began. "May I gather with those under your protection?"

"No, you can't be trusted and I will not risk my charges' safety," Sojourner's
eyes constantly scanned the cavern.

"But I deserve safety as much as they do," Enna said.

"You rob other slaves. You deserve nothing from me," Sojourner replied.

"I need the safe, clean bunks of your cell," Enna whined.

Sojourner turned away.

*'Why did you stop yourself from explaining?'* we asked. *'What were you going
to say?'*

*'I can't say,'* she replied. *'You can't make me.'*

She sobbed hysterically.

***

The next day, our hostess regained her confidence. She ate heartily and foraged
efficiently. She determined to escape the
Salvage Caverns. Her change in attitude seemed unnaturally fast for a widow.

As she scavenged, a new slave appeared from Elsewhere. He was in a clear tube of
a hard substance. Enna dug him out of the rubble and pressed a green button on
the tube. Hissing escaped the capsule and the unconscious man, in strange
clothes, awoke.

Sojourner came to help him out of the capsule.

With her mental powers, Woethief scanned the man's mind, but he had no memory of
his past. Because of a marking on his capsule, Sojourner called him "Four".

Through her mental powers, Enna made the new man learn the Darkish tongue
instantly. He joined Sojourner's group and seemed particularly fond of her.

When Enna rummaged through the capsule, she found a medical kit and pamphlets.
We studied every page, though we did not know the language.

With the medical kit, Enna treated the wounds of the slaves in Sojourner's
group. She hoped to earn trust.

The pamphlets were sent to the National Library, which happened to be the only
library in the country.

As Four grew closer to Sojourner and charmed her with his kindness, the gray
guard grew jealous. He shook his head at Four, but couldn't do anything about
him, since Sojourner would not let the two men near enough to talk.

Our hostess approached the gray guard to ask him about Sojourner. The guard did
not talk. Something about him was familiar, but our hostess kept us from
remembering.

When Sojourner announced that she would marry Four in three days, the gray guard
panicked.

Since two days from that day would see the mass "coupling", where any singles
would be married to spouses of the guards' choosing, the gray guard hatched a
plan.

On the "coupling" day, Sojourner assigned slaves under her care to each other,
but was called away to break up a fight. Her group consisted of the only kind
and gentle people in the Salvage Caverns. The gray guard took Enna by the hand and led her
to Four.

"Please don't make him marry me," Enna pleaded. "I'm crazy. I already watched my
first husband be killed."

The gray guard said nothing.

"Sir," Four said. "Please do not do this. You know that I love Sojourner."

The gray guard wept as he applied a tattoo to Four's cheek. He wrapped his hand
around Enna's shoulder and his palm touched the shard. Our minds instantly
connected to his.

*'Den,*' WoeNyl thought. *'Is that you?'*

*'WoeNyl, how are you here?'* he asked.

We explained how we had come there as he added a star to the end of Enna's
tattoo, to signify remarriage.

*'Why are you doing this to Four?'* Woethief asked.

*'Sojourner is Theila. I can't let my wife marry someone else,'* Den thought.

As our brother-in-law released Enna, we lost mental contact with him.

*'What happened?,'* Enna asked. *'What did you say to him?'*

*'He is our brother-in-law and Sojourner is our sister,'* WoeNyl replied.

When Sojourner returned to the courtyard, hot tears burst from her eyes.

"How could you?" she yelled and punched Den in the left eye.

Den took the blow in silence. In our past life, he was cursed with muteness for
helping us. We longed to comfort him and convince our sister that he was
protecting her.

*'Please allow us to inhabit Den or Sombermirth,'* WoeNyl pleaded.

*'Who is Sombermirth?'* our hostess asked.

*'Theila Sombermirth is Sojourner's real name,'* we thought. *'We want to help
her.'*

*'I need your help too,'* our hostess thought.

*'We would still help you,'* we offered. *'Living in a guard would give us more
opportunity to help.'*

*'I won't let you leave,'* she declared. *'You're mine. I found you.'*

She continued her day as part of Theila's group. In a rare act of kindness from
the guards, the newlywed couples were given extra food and only required to work
half a day.

Despite his broken heart, Four made the best of his situation. Learning that
"Sojourner" and Den were married blunted his heartache.

Enna eagerly accepted her handsome husband, since he brought her into Theila's
group, which had the best living conditions.

We blocked our ability to feel her senses, when she and Four spent time together
in the cell. When we couldn't sense the outside world, we felt so alone.

*'God,*' WoeNyl prayed. *'Please let us live in Den or Theila. We feel trapped.
Soften our hostess's heart.'*

Whenever Enna injured herself in her work, Four tenderly cared for her, which made Theila even more heartbroken. She knew
what she was missing. We prayed that Theila would let Den care for her.

Theila and Four both mourned their inability to remember their past lives. Our
hostess failed to restore Four's memories with her telepathic powers.

With each week, Nyla's healing caused our hostess's power to grow. One day, as
Four ran his fingers through her hair, he felt a soft growth.

"What's this?" he whispered, since it was after lights-out.

"I don't know," she said. "But since it started growing, I've wanted to be in
the light."

"That's not the only thing that's changed," Four said. "You get stronger and
more beautiful every day."

He placed his hand on her breastbone and we forced past our hostess to connect
to his mind.

*'I'm healing her,'* Nyla thought. *'I'm causing her powers to manifest.'*

*'What's happening?'* Four asked. *'Who are you, voice?'*

*'We are the silver shard. We care for your wife and her child,'* WoeNyl
thought.

He withdrew his hand and our contact broke. We needed to talk to Den or Theila.
We missed our family.

Over the next weeks, our hostess's growths spread until her hair had a
literal crown of green tentacles. The tentacles had green things, which were
shaped like teardrops.

With Nyla's help, she created a sweet, edible organism that grew near torches
and the light-shadows from the portals to Elsewhere. Slaves from other worlds
called the stuff "moss". They called her tentacles, "vines", and the teardrops,
"leaves".

The moss nourished the slaves and brought healing to their bodies.

*'She's jealous,'* our hostess thought about Theila. *'She's always scowling at
me.'*

*'You married the man she thought she loved,'* WoeNyl replied.

*'It's not just that,'* our hostess replied. *'She wants my power.'*

*'Let me talk to her, please,'* Nyla begged. *'She's my sister.'*

*'I don't trust you not to run away from me,'* our hostess replied. *'If she had
control of you, she could wield you against me.'*

*'How much longer must we serve you before you free us?'* Nyla asked.

Our hostess refused to answer. At least, her husband's great devotion and care
stopped her erratic mood swings.

<!--Unborn child is WoeNyl's too. -->
While we worked, Woethief tried to project her mind toward Den and Theila, but
our hostess blocked her. We most wanted to talk to Theila's unborn child. We
prayed that we could be reunited with our family.

Despite our hostess's improved behavior, Theila distrusted her.

Den ensured that no one hurt Theila or the slaves in her charge. His sad eyes
pondered her with longing, which his mute mouth could not express.

Shame for forcing Enna and Four into their marriage tormented him. Woethief
longed to steal his woes, but he resisted her efforts.

The presence of our hostess amplified our powers. Woethief felt the bad emotions
of everyone around us, but could do nothing to soothe them. Nyla kept Woethief
company when our hostess cut us off from her senses.

*'Who are you?'* Nyla asked our hostess. *'You are powerful like us, so you must
be a stewardess.'*

*'What is a stewardess?'* Enna asked.

*'A stewardess is a powerful being assigned to protect a world,'* Woethief
explained. *'We were the daughter of stewards and married another steward in our
world.'*

*'You are married?'* Enna asked. *'You never mentioned that.'*

*'We are divorced,'* Woethief replied.

*'That makes sense,'* Enna thought.

*'What does that mean?'* Nyla wondered to ourself.

She did not answer our questions about her life, but constantly changed the
subject. We figured she was a stewardess, but we did not know what world she was
entrusted with. Perhaps, she was a wandering, worldless stewardess like us.

***

When the guards discovered our healing power, our lives changed. They retasked
our hostess with mending injured workers.

Rather than protect the slaves from injury, the guards worked them harder and
longer. As long as Nyla had a steady water-supply, she could heal injuries, even
replace missing limbs. The cost of her gift was that any area we healed was a
copy of Nyla's old body, before it turned to silver.

The long hours exhausted us, but we kept our hostess healthy. Theila, Den, and
Four were taken from their posts and assigned as Enna's bodyguards.

Without Theila's protection, her old charges returned to squalor. Theila hated
our hostess for taking her away from where she was helpful. She especially hated
working with Den, who was her undeserved nemesis, and Four, whom she still
loved.

Because she used Nyla's powers, our hostess became "rich" in the Salvage Caverns. She
slept in her own quarters, decorated with furniture that washed through the
portals.

Four shared in her climb, but not in her arrogance. Because of Theila's words,
he embraced our religion, the Way. Enna did not
deserve such a true husband.

***

Word of the healer (our hostess) spread outside of the Salvage Caverns. A gaggle of nobles
visited and demanded a demonstration of the power.

A noble shoved Theila against a wall and held her there, while another man drew
a whip.

Den swung his fist into the noble's jaw. The enraged nobles seized Den and tied
him between two pillars.

"You beat him," the noble said, as he pressed the whip into Theila's hand. "Or
we'll kill both of you."

Theila looked into our hostess's eyes, pleading for help. Nobles held Four back.

*'You must help Theila,'* we thought.

*'I can't,'* Enna replied.

*'Confuse their minds,'* we begged. *'Stop this evil.'*

*'I can't,'* Enna repeated.

To protect the daughter in her womb, Theila swung the whip. The nobles kept her
at spearpoint until she had whipped Den into unconsciousness.

Tears streamed down her face and shame became an aura around her. Sweat drenched
her.

"Now, let's see you heal him," a noble said to our hostess.

"I need water," our hostess replied. "Let Four retrieve it for me."

They released Four, who poured water from a pitcher over Den's bleeding back.
Nyla's power flowed with the water and erased Den's injuries. Since he and Nyla
both have gray skin, her healing left no scars. Our hostess prevented us from
healing Den's voice.

"We've seen enough," a noble said. "You'll come with us to the capital."

Our hostess packed her things. When Four, Den, and Theila tried to come along,
the nobles protested.

"We only need you," the leader said.

"I will not go if my husband cannot come," Enna said.

"You won't need him anymore. You have me," the leader stroked her face.

"Ask the salvagers. My power appeared after I married him. His kiss is magic,"
she said.

"We'll see about that," the leader said.

He ordered Four and Theila to kiss, which they did at sword-point. Our hostess
was proved a liar, when Theila did not gain powers.

As the leader dragged her away from her spouse, Enna manifested a new power. She
transformed him into a feeble old man.

"My husband and I will not be separated," she announced.

The leader fell to his arthritic knees and begged for mercy.

"Swear your allegiance to me," our hostess demanded. "When you are ready to
serve me, I will heal you."

No one protested when Four and our family followed the nobles out of the Salvage Caverns.

Despite how the leader had abused him and Theila, Den helped him on the long
march. When the old-bodied man weakened, Den carried him. We loved Den for his
kindness.

During the week-long march, Nyla focused her ordering and healing powers on the
old man.

Water in the air allowed Nyla a tenuous connection to him, so she could heal his
fragile skin when he scuffed it on rocks. Our ordering lessened the effects of
his age. Although he looked no younger, he felt five years younger by the end of
the week. Curls like Nyla's sprouted from his bald head.

*'Why are you healing him?'* our hostess asked. *'You're destroying my
leverage.'*

*'His actions toward my brother-in-law and sister were cruel, but do you have
the right to punish him so severely?'* Woethief asked.

*'Of course I do,'* she thought. *'I am the goddess of this world.'*

*'Do you mean "stewardess"?'* Nyla asked.

*'That term doesn't capture my majesty,'* she replied.

*'You were no one until we helped you,'* Nyla thought.

*'But now, I am on the rise,'* she turned up her nose. *'Know your place,
slave.'*

We stopped thinking to her. We did not want to anger her.

***

*'WoeNyl,'* Nectarfang thought to us. *'Can you hear me?'*

*'Yes,'* our sleepy minds answered.

*'I don't know where you are or if you can hear me,*' Nectarfang thought. *'But
I love you. I can feel the treblets moving inside of me. It's wonderful.'*

*'I can hear you,'* Woethief replied.

*'I wanted to thank you. You made me so beautiful and powerful. I'm making
friends. Flame lives in Faithful's old house. So does her step-mother.'*

Despite our replies, Gemma did not seem to hear us.

*'I love you,'* Nectarfang said. *'Come home, sister.'*

Joy, so strong that it made our hostess weep tears of joy, filled us. We missed
our twin sister, but her kind words overwhelmed us. Turning to silver was worth
it for Nectarfang to be given a new chance at life.

***

When we reached the surface, plants blossomed and vines erupted from cracks in
the stone buildings. Our hostess's power to create plants was uncontrollable.

The people in the capital, who always lived on the verge of starvation, welcomed
the edible vegetation.

A veil of vines hid our hostess's face. People whispered of another vine-person
in the Temple-Palace.

<!-- Lengthen explanation of setup.-->
Because of her power to transform the leader, nobles set our hostess up in a
minor shrine and charged money for restored youth and healing.

The old and wealthy came to our hostess and she gave them youth for a steep fee,
which enriched the nobles.

The leader insisted that our hostess be entirely shrouded, so people could not
identify her.

Den and Theila's weapons were confiscated and they became criers for our
hostess's services. Den's bright-red sign looked ridiculous on him.

Over the next month, the leader grew younger and grayer.

Our hostess sneaked out whenever she could to find the crippled and homeless.
Since she healed them at no cost, she won their loyalty. Whenever she told them
where to go for aid, she blocked WoeNyl from hearing what she said. She was
raising a private army.

The guards were easy to bribe. Rather than give money, which she had none of,
she gave them something more valuable: muscle. The guards became unnaturally
strong, quick, and dexterous.

When the nobles discovered that she could recreate bodies, rather than just heal
them, they charged for that service. The guards became specimens to show off.

Den and Theila did not escape involuntary transformation. Their beauty and
strength surpassed natural limits.

Enna's power only worked on those with Remnant or Dryad (the race, which gave
her her vines) blood. Since Four was from another world, Enna could not change
him.

***

One day, our comfortable life was interrupted. A large faction of Remnant people
hate extranatural powers. Centuries earlier, they murdered their stewards, to
become their own masters. Their world fell into disrepair because the dying
stewards cursed it.

Until the last few months, all the world was black stone and most plants were
thorny. All pleasant things in the natural world were dulled and all painful
things were sharpened.

The Temple-Palace held the mummified corpses of the stewards, who were kept as a
warning to those with extranatural powers.

A mob formed outside our shrine, demanding that Enna to be brought out.

The leader of the nobles went out to talk with them, but they beat him to the
ground before he could say anything. Despite his lack of weapons, Den charged
out and dragged the leader back inside.

Our hostess poured water on the leader's bloodied face and Nyla healed him. Afterward, he
had a patchwork face with gray scars maiming his dark brown skin.

He knew when to cut his losses, so he offered a secret exit from the shrine in
exchange for being young again. Our hostess took the offer and made the leader
young, strong, and handsome. She could not remove the gray scars.

We left through an underground tunnel with an escort of 20 guards and four men
to haul a cart. We headed for a portal to a tamer world, called, "Greenplains".

At the edge of the city, the leader's wives and children met us. Under cover of
darkness, we embarked on a journey that should take a month.

We left a trail of green behind us, as our hostess always created plants in a
100 meter radius around herself.

Those, who wanted to kill her for her powers, were satisfied with her
banishment, so they did not pursue.

The leader of the nobles chuckled to himself for having outsmarted his
subordinates. He wouldn't need to split the profit now that they were left
behind.

***

Three days into our journey, our hostess asked me a startling question. *'When
are you going to take over my life?'*

*'Why would we do that?'* WoeNyl asked.

*'I'm very powerful and beautiful and my husband is very attractive,'* she said.

*'We are happy for you. We are content with our own power. We have no intention
of running your life,'* Nyla said.

*'Why would I want to ruin your happiness?'* Woethief asked.

*'But, you could if you wanted to,'* Enna thought.

*'We do not know,'* WoeNyl replied. *'We have been mind-controlled, so we would
not wish that pain on you. If you are worried, release us.'*

*'I can't,'* Enna said and sniffled.

***

That night, a multitude of hostile minds surrounded us.

*'Wake up,'* Woethief thought to our sleeping hostess, but she thought
Woethief's voice was part of a dream.

We had told her we would not control her mind, so we could not break our word,
even to save Enna's life. Nyla is truthfulness embodied.

As we tried to awaken her, a clay flask landed on the wagon and set it alight.

The women and children jumped from the wagon as arrows rained from the hillside
around us. The cheated nobles had come for revenge.

*'Get them under a rock,'* Nyla ordered our hostess.

She hurried the children and Theila into cover. The leader of the nobles fell,
along with most of the guards. We shrouded them in twilight fog.

Because of the darkness and surprise, the guards could not defend effectively.

We noticed a rifle on the ground, but before our hostess could grab it, Four
picked it up. Despite the weapon's rust, Four hit three enemies with the last
four bullets. It was the only firearm in our party.

The enemy corralled us survivors into a circle. One wife, three children, two
cart-men, and mine and our hostess's families held up our hands.

"He thought he could stiff us," a noble roared. "We taught him."

"We just followed orders," a cart-man said. "None of us planned this."

"Kill the guards," the noble said. "Only a coward would surrender."

"We're not guards," a cart-man announced.

*'Think about thorns,'* Nyla ordered our hostess.

*'Why?'* she asked.

*'We can trap the soldiers in thorns,'* Nyla replied.

The enemies screamed as thorns sprouted from the ground and entangled them.

"Take their weapons," Enna ordered the cart-men.

We burned whatever bows and spears we couldn't carry, but kept smaller arms. The
cart-men and leaders' family wanted to return to the capital, but we wanted
freedom in Greenplains.

We parted company after Nyla healed the wounded. One of the children, who was
burnt, now bore a replica of Nyla's face, including her eyes, which saw heat.

*'Show pity to our enemies,'* Woethief pleaded. *'We must not leave them here to
die.'*

Enna created a plant with foul-tasting, gray berries.

"In a day, the thorns will release you," Enna said. "If you eat a berry, your
wounds will heal, but your muscles will shrink."

The enemies moaned. Being weak was worse than death to them.

She placed a seed on each dead body.

"If you bury the bodies, these seeds will sprout in a year," she continued. "The
new plants' berries will heal you of your weakness."

We left with whatever supplies we could scavenge. Without the cart, marching a
pregnant women for a month was daunting.

Despite the late hour, Enna was energetic. Nyla kept her in perfect health. No
one would have known she was as gaunt as a corpse a few months earlier.

"It will be too easy to follow us if they send more men," Theila said. "Can't
you stop making plants."

"No," Enna said. "It's automatic."

"Then, can't you make thorns behind us to slow them down?" Theila asked.

"Yes," Enna replied.

*'If you are the stewardess of this world, do you really want to curse your
people with thorns?'* Nyla asked. *'They will spread and afflict many
innocents.'*

*'Don't worry,'* Enna said. *'They'll be seedless.'*

Enna did not empathize with the pain of our party as jagged rocks sliced through
their shoes. She thought that because Nyla healed them each night, their pain
was meaningless.

She insisted on guiding our party, but we deviated from the map the noble had
shown us. Enna insisted we were being pursued, though Woethief sensed no one
nearby.

*'Do you feel it?'* Enna asked.

*'Feel what?'* Woethief thought back.

*'There's power coming from Theila,'* she replied. *'It's even greater than your
power.'*

*'When I try to reach for Theila, you stifle my mind,'* Woethief lamented.

*'Theila is changing,'* Enna thought.

Though she did not specify the change, we found out soon enough. In addition to
her belly swelling with her surrogate child, Theila's skin was lightening and
strange growths emerged from her head and below her navel.

"What are you doing to me?" Theila yelled at Enna.

<!-- Is morning sickness different from morning sickness.-->
The pain of her transformation compounded her sore feet and morning sickness.

"I'm doing nothing," Enna said. "The power is in you."

"I'm not Remnant anymore," Theila said. "I have these hideous pocks all over."

Den did not think her beige skin or freckles were hideous.

"I can only make people into dryads or Remnant, not whatever you are," Enna
said.

Because of our pity for Theila, we helped Enna to rechannel her creativity. She
created varieties of new of plants with juicy fruits to hydrate us. They broke
up the monotony of her previous moss and thorn pears. Soft grass sprang up to
sooth the party's feet.

***

One morning, Theila awoke with no feet. They had not been cut off, but stubs
were in their place. To her exasperation, Den was forced to carry her on the
march. She feared him like he were her archenemy, but he only showed kindness to
her.

Holding his wife made Den miss her more than ever. We prayed that she would
remember her own identity and love for Den.

<!--Explain Ild ears.-->
Over the next week, Theila's hair turned red and Ild ears sprouted from her
head. Her human legs disappeared completely, to be replaced by eight
spider-legs.

<!--Why didn't she turn into a Lacerator? Explain how or why Crystal is doing this. -->
Theila was now a spider, mixed with a race that called themselves, "Merchants".
She kept her Remnant muscle.

Den held her hand as she learned to use her new legs.

In the middle of the day, we crested a hill and our hostess ordered everyone to
halt. She pointed her captured gun at each person in turn and forced Four to
bind their hands. She bound Four herself.

"Why are you doing this?" Theila asked.

"I can't tell you," our hostess said. "We must wait here."

"Why?" Four asked.

"You know I am a dryad," our hostess said. "We are near the portal to my world."

Our hostess's finger trembled over the trigger as she aimed at her husband.

*'I don't want to do this,'* she thought to WoeNyl. *'Stop me.'*

With her permission, we seized control of her body and dropped the pistol.

"We have taken control now," WoeNyl announced through Enna's voice. "We are Nyla
and Woethief. Our silver shard is in her throat."

"What is a woethief?" Theila asked.

"It is our name. We are your sister," we said, as we stooped to untie Theila.
"Your real name is Theila and Den is..."

We lost control of the body. Our hostess bowled Theila over and reached for the
pistol. Den stepped in front of Theila to shield her.

"What is happening?" Four asked.

"It's time to gain the power I need," our hostess announced.

As she sighted on Den and Theila, Four plowed into her. The pistol fired and
struck him in the thigh.

"I'm sorry," our hostess sobbed as she fled.

*'Why did you do that?'* Nyla demanded.

*'I can't tell you,'* Enna replied.

*'You think Theila's power will transfer to you if you kill her,'* we thought.

*'I can't tell you,'* our hostess replied.

*'I will not let you hurt our family,'* Nyla warned.

*'I am sorry, but I must know what is happening,'* Woethief thought.

Woethief concentrated on Enna's fear. As Woethief focused, the thing that scared
Enna came to the front of her mind.

*'She's making you act this way?'* Woethief asked.

*'I can't say,'* Enna replied.

*'Since you are an identical twin, Ella
went to live with Prince Vehemence instead of you,'* we thought.

*'I can't...'* Enna began.

*'You really love Four and Theila,'* we thought.

Woethief pushed as hard as she could against Ella's mind to free Enna.

*'What took you so long to figure it out?'* Ella asked.

*'We did not want to invade your mind like that,'* Woethief thought.

*'Please let us alone,'* Enna begged.

*'You will go through the portal,'* Ella announced. *'Each of us has half of the
Seed. When I plant it, I will claim the throne and my tree will shadow the
land.'*

Ella forced her sister's body to keep walking, but left her mind alone.

*'What will happen to my husband,'* Enna asked.

*'I don't care,'* Ella replied.

*'Theila is a spider, so she can bind Four's wound with her silk,'* Woethief
suggested.

After walking for three and a half hours, we came to a cleft at the bottom of a
ravine. Without knowing it was there, the portal in the wall would be nearly
impossible to find.

***

On the other side, a green world of lovely forest met us. Hundreds of bird
species sang to us from the canopy. Since we were born in a cave, the forest
surprised and awed us.

Ella stood before her sister, wearing a shapely dress of green velvet. Forty
armed men accompanied her. She had transformed Remnant and Dryad men into
fearsome and beautiful hybrids.

Woethief longed to embrace Ella, feeling the sisterly love that Enna should have felt. Nyla held us back.

"Greetings, majesty," a wizened dryad bowed to Enna. She looked to be seventeen
and seventy at the same time. "Your other self said you bear half of the Seed."

Enna plucked a translucent, green semi-ellipsoid from her hair.

*'Do not tell the elder we are twins,'* Ella warned. *'She thinks we are one
person with two bodies.'*

Because of Ella's mind-control, we did not share the news.

The youthful elder led Enna to a hut, where she dressed her in green velvet.
When we emerged, Enna and Ella joined hands and walked into a clearing a
kilometer wide. A rotten stump uglified the clearing.

"Remnant villains burned our tree 50 years ago. Only one seed survived," the
elder announced to the crowd. "Now, the Seed has returned. I did not expect it
to be born as Remnant women, but I welcome the return of our tree."

The throng of dryads waved their hands in the air at the same time that all the
trees in the forest shook their branches. The "applause" sent millions of birds
into the air in intricate formations.

"Now, the Seed will be planted," the elder announced.

*'We will rule together, won't we?'* Enna asked.

*'I have a better plan,'* Ella replied. *'Kill you.'*

Woethief felt Ella's malice. Could she kill her sister's mind?

*'Ella please make this right,'* Woethief begged. *'You and Enna can start over
and reign together. Your combined beauty can be the joy of your kingdoms.'*

*'I need no companion,'* Ella thought. *'My beauty is sufficient for both
worlds.'*

*'What about us?'* Nyla asked. *'If we are attached to you, will we become part
of your tree?'*

*'I can't risk it,'* Ella answered.

She forced Enna to pluck out our shard and hurl it to the ground. As our senses
faded, we called out to any nearby minds.

***

A green mink answered our call. Since it was a soulless beast, we gained control
of its body when it touched our shard.

Ella and Enna held out their hands as the elder pricked their thumbs with a
quill. They fused both halves of the Seed with their blood and dropped it onto
the stump. A tree trunk twisted around and lifted the sisters into the sky as it
grew.

Woethief sensed Enna's mind fighting to stay tethered to its body. If the tether
broke, she would enter the after-death.

With our nimble body, we scaled the tree and prayed that we could reach the top
in time.

Panting, we gained the top and grabbed Enna's leg with our paws. As her soul
detached, we grabbed it.

*'What happened?'* Enna asked. *'Why am I so small?'*

*'We put your soul in a mink's body,'* WoeNyl replied.

With water from a cloud, we made the mink slightly bigger.

*'Find a pond or a stream,'* Nyla ordered.

Enna sped down the trunk with her claws gripping the bark. Her sister used her
telepathy to force the crowd to hunt for us.

We dodged between legs and around trees. Every drop of water we met increased
our mass. Thankfully, there was not too much water or we could not have slipped
through the crowd.

The trees sent their roots after us like tentacles, but we narrowly dodged their
grasp. When we finally reached a lake, an alligator met us.

*'What do we do?'* Enna asked.

*'Jump,'* Nyla ordered.

She bounced off the reptile's snout into the water. When we touched the lake,
Nyla used her creative energy to morph Enna into a humanoid shape.

The alligator snatched her leg with its teeth, so she dug her claws into its
eyes. As she succumbed to the monster's strength, we used our power again. Our
shard embedded in the alligator.

While controlling the reptile's body, we released its jaws from Enna's leg and
carried her across the lake on its back.

At the other end, we reconnected our shard to Enna and limped onto the bank
where the blinded gator had a hard time following.

*'I still know where you are,'* Ella thought.

We climbed a rocky hill, with no trees, so we could not be entangled by roots.

"What will we do?" Enna tested her new voice.

*'We must find our families. The only way out that we know of is the portal,'*
Nyla thought. *'I wish we had made you a bird.'*

"What will Four think when he sees me?" Enna worried.

*'He's a kind husband,'* WoeNyl replied. *'He will love you even in your new
form. We did our best to make you beautiful when we created you.'*

We healed her leg, then remembered Four's injured leg. What if he bled out from
his wound?

*'How did Ella trick us?'* we asked.

*'She controlled my mind,'* Enna replied. *'Since we were born, we were
connected. She controlled my mind, but whatever I ate or drank nourished her
too. When you healed me, she became beautiful too.'*

*'Where was she?'* Nyla asked.

*'She hid in a cave for years. She didn't need anything brought to her because I
ate for her. She never got bored because she could control my body for fun.'*

*'Our sister did the same thing,'* Woethief replied. *'She changed after we
saved her life. She loves us deeply now. Ella can change too.'*

*'Sombermirth hurt you?'* Enna wondered. *'She seems sweet to everyone but
Den.'*

*'No, our other sister, Nectarfang did,'* we replied. *'Now, she comes to us in
dreams and gives us new powers. We miss her.'*

***

At night, Enna crept through the forest. Her animal instincts made stealth
natural as she crept from shadow to shadow.

100 meters from the portal, an unseen trap cast a net over her. As she
struggled, it tangled her more.

"We caught her," a soldier whooped. "She's a looker too."

We wished we had made better clothes than the leaves that Enna had weaved together.

The dryad elder approached the net.

"Please, may I borrow your cloak?" Enna asked.

"It is a ceremonial piece passed down to me..." the elder said.

"Please, I am a helpless woman surrounded by men, show pity," Enna interrupted.

The elder unfastened her cloak and draped it over Enna.

"I will free you from the net if you promise to come with me peacefully," the
elder said.

"I will if you have the men leave first," Enna said.

The elder ordered the soldiers to turn their backs to Enna and released her.
With the cloak, Enna looked regal.

To our dismay, Enna darted for the portal.

*'If you break your promise, we will leave you,'* Nyla thought.

*'Why should I stay here?'* Enna asked.

*'We will do what we can to protect you. Have we not already protected you?'*
Nyla asked.

Enna turned and walked back to the elder.

"Wise choice," the elder said. "The traps get nastier, closer to the portal."

<!-- Did they anticipate her coming?-->
Enna followed the elder to a hut, where fresh clothes waited. A meal of cheese,
bread, salad, and apple cider invited Enna.

"Is this poisoned?" Enna asked.

"Of course not," the elder said as she took a piece of cheese. "It is Ella's
best."

After the elder ate a bite, Enna enjoyed the meal, especially cheese, which she
had never tasted.

<!-- Explain that she is a regent.-->
"I am Meredith," the elder said. "I hid Ella's seed from the invaders 50 years
ago and now it has returned to its rightful home."

"I am Enna and this is Nyla Woethief," Enna motioned to the shard. Something in
the cider had loosened her lips, though the drink was non-alcoholic.

"Where is my husband?" Enna asked.

"He is with Ella," Meredith replied. "The gray man and spider are too."

"Why am I here?" Enna asked.

"I want you to meet my goddess, but I knew you would be hungry first," Meredith
explained.

The elder stood and motioned for Enna to follow.

We walked through moonlit forest toward Ella's tree. It dwarfed all other trees.
In its presence, we felt a beauty we had never known in Woethief's underground
homeworld.

*'Your sister's tree is splendid,'* Woethief thought.

*'It should be both of ours,'* Enna replied. *'It is mine by right.'*

*'Yes,'* Woethief thought. *'We will try to convince Ella to restore your body
to you.'*

*'She won't listen,'* Enna sulked. *'Ella's a monster.'*

We wished for our own body. Talking to Ella would be easier if we could look her
in the eyes.

## Confrontation

At the base of Ella's tree, Meredith simply walked into the trunk. With shaking
knees, Enna did the same.

The wooden interior was "carved" ornately with images of plants and animals. Well-arranged
furniture filled the room and a spiral-staircase led upward. Everything (stairs,
walls, and furniture) was a contiguous piece of living wood.

We followed Meredith up the stairs into the throne-room. Ella sat, clothed in
living vines. Her majesty overwhelmed Enna, who prostrated herself before her
sister.

"Please, show mercy to me. Release Four," Enna begged.

"He is safe, worm," Ella said. "Stop groveling and face me like a woman."

"Your worshipfulness, if I may..." Meredith started.

"You may not," Ella said. "Enna, your husband is safe and will remain so if you
follow my instructions."

"What do you want?" Enna asked.

"You don't know how to use it, but you still have power. Give it all to me and
promise that you and your heirs will never try to take my thrones here or in the
Remnant Lands," Ella demanded.

"I won't give you any more than what you've already taken," Enna said.

A wooden cage came up around Enna. Before it trapped her completely, we detached
our shard and clattered on the floor. Woethief called out and Meredith picked us
up.

*'Please allow us to speak to her majesty, Ella,'* we thought to Meredith.

The elderly dryad dropped us.

We prayed for our captive family and friends as we waited in our prison of
senselessness.

***

We awoke and blinked eyes that we controlled. We saw Ella's overwhelmingly
lovely face and the cage surrounding Enna.

"You are awake: good," Ella said.

"How are Den and Theila faring?" Nyla asked, suppressing Woethief's panic.

"They are trying to escape," Ella chuckled. "They can't of course, but they are
fine. I have them in a comfortable room and Meredith will bring them a meal
shortly. Her bread is quite good."

"May we see them?" Woethief asked. We could not move.

"That depends," Ella said as she approached us. She put her forehead against
ours. "Do you intend to harm me or use your power against me?"

"We would only hurt you if it were necessary to protect the innocent," we said.
"Let our family and friends go and you will have no reason to fear us."

"To protect myself, I must confiscate any power they have," Ella said. "I do not
let people use power against me."

"Free us and at least our family will have no cause to be your enemies," we
said.

"But Enna hates me," Ella said.

"Then give her body back and share your rule with her," we suggested.

"Her body is already in the Remnant Lands claiming my throne," Ella said.
"Besides, she will never forgive me for what happened to Joel. I could have
stopped Vehemence."

"Ask for her forgiveness," Woe said. "We forgive you for trapping us. Enna may
be able to forgive you too."

"I cannot be safe while they know what I did," Ella sighed. "They know my real
past. My enemies would pay them for that information."

In the reflection of Ella's eyes, we saw that our "body" was a potted plant,
shaped like a human head.

After their meal, Meredith led our family and Four into the throne room.

"Den, Theila," we called. "It's us, WoeNyl."

Den approached and touched our shard to be sure it was real.

"We missed you so much," we said. "We love you."

*'How did you escape Ildylia?'* Denrick wondered.

*'Stop talking,'* Ella ordered our minds. *'Or there'll be consequences.'*

Den withdrew his hand.

"Den and Theila," Ella said. "You are free to go, but I will keep your memories
and any power you have." Ella touched each of their foreheads and they fell
unconscious.

"I sense much power in the little one," Ella touched Theila's abdomen.

"Do not harm our daughter," Nyla ordered.

"Your daughter?" Ella asked.

"Yes," we said. "Theila is a surrogate mother. The child is ours and our former
husband's. Have we not shown kindness to you? For our sake, do not harm our
child?"

"You are bold to ask," Ella glared down at our head.

"We do not want you to harm Den or Theila, but we are powerless to stop you,"
Woethief said. "Please show mercy at least to the infant."

"You have been helpful to me and only mildly interfered with my plans," Ella
said. "I will grant your request if you convince Enna to give me her power."

The cage around Enna disappeared and she raced to her husband.

"Four," Enna said. "It's me. Is your leg alright?"

"Meredith healed it with a leaf," Four said. "Who are you?"

"We put your wife's soul into a mink," Woethief said.

"That is enough," Ella interrupted. She touched Four's brow and he fell
unconscious too. Wood gently lowered him to the floor.

"Enna," we said. "Will you please give your powers to your sister? She will
leave Theila's daughter with her powers and memories intact if
you do."

"Whose side are you on?" Enna snapped.

"Our family's," WoeNyl said. "This is your best way out of this situation."

"She'll feel my claws," Enna said just before vines bound her wrists.

"Ella, will you give us safe conduct to Greenplains?" Nyla asked.

"Yes, if I get her power," Ella said.

The drug that Meredith had given to Enna weakened her resistance.

"Fine," Enna wept. "Take my power."

Ella stole her sister's powers and memories. Enna slumped.

"Where does that leave us?" Nyla asked.

"You may connect to Theila," Ella said. "I know you will want to be there for
the birth of your child. But if you try to use any of your power except for
talking to Theila, all your daughter's power will transfer to me."

"We understand," Nyla replied.

"We have one last request," Woethief added.

"What is it?" Ella asked, annoyed.

"Please give us arms so we can hug you before we leave," Woethief said. "You
have been kind to us. Thank you for sparing our daughter."

Ella gave us a humanoid, plant body. We walked to her and felt her nervousness.
When we hugged her, we said, "We love you, Ella. We hope we have earned your
trust."

We touched Ella's abdomen, which contained a boy-child. "Child, if you ever need
a friend, find us and we will do what we can to help you for your mother's sake.
May you be as beautiful as her and as kind as Den. May your enemies grow to love
you and your friends be loyal."

"You are a strange creature, WoeNyl," Ella said. "You may go to Theila now."

Our shard embedded in Theila's flesh and we felt at peace.

*'Theila,'* we thought. *'We know you don't remember us, but we're your
sister.'*

*'I don't remember a sister,'* Theila replied. *'Am I dreaming?'*

*'Yes,'* we thought back. *'You are married to Den and he has been completely
faithful to you. Trust him.'*

*'He frightens me,'* Theila thought. *'Why would I have married him?'*

*'To fulfil an oath,'* Nyla replied. *'We will do everything we can to help him
win your love.'*

Fear filled Theila. Woethief wished she could steal the fear, but we had
promised Ella not to use our powers.

We fell asleep in the safety of our sister's mind.

***

We awoke in the Remnant Lands, away from the portal. All four adults did not
remember who they were or why they were there.

*'Theila,'* Nyla thought. *'You must take charge. You all must get to
Greenplains.'*

*'Is this a dream?'* Theila asked again.

*'No,*' WoeNyl replied. *'We are your sister and we can talk to your mind to
help you. Den is your husband.'*

*'Who is Den?'* Theila wondered as she rubbed her eyes.

*'He's the gray-skinned man,'* sadness tinged our reply. We hated that we needed
to tell Theila who her groom was.

We explained the party members' names. When we tried to share their backstories,
Ella stopped us.

*'I'm keeping Crystal's telepathy from reaching your family and you
can't share their pasts with them,'* Ella thought to us.

*'We understand,'* Woethief replied. *'If you ever change your mind, we will do
all we can to bring peace between you and Enna.'*

As Theila surveyed our equipment, she found a map that led from our position to
the Greenplains Portal.

"Listen," Theila said. "We must reach Greenplains as soon as possible. I don't
want to give birth in this wilderness."

"Who are you?" Four asked.

*'Tell him you're Theila Sombermirth and that you're Denrick's wife,'* Nyla
suggested.

Theila shared her full name and explained as much of each party member's
identity as Ella allowed. She omitted her relationship to Den.

*'Why did you not tell him?'* Nyla asked Theila.

*'He scares me,'* Theila replied.

The party embarked on the on the two-week march to the Greenplains Portal.
Theila was almost nine-months pregnant, so travel was slow.

The razor-like rocks punctured their shoes and made sleep fitful. The provisions
from Meredith dwindled more quickly than anticipated.

Enna and Four left most of the food behind and rushed to the Greenplains Portal
to get help.

Theila hated being left with Den, but needed a slower pace. Den went down to one
meal a day, so his wife could have full rations.

An aura of gratitude emanated from Crystal when she realized Den's sacrifice.
Though we could not use our powers, we could not turn off our connection to
Theila's senses or our daughter's aura.

At noon, Theila's water broke. She and Denrick sheltered in a cave, which had a
stream. Denrick trembled as he realized he would need to help Theila on his own.
Theila dreaded the embarrassment of letting Den see parts of her that were
normally hidden.

*'He's your husband,'* Nyla thought. *'There is no shame in letting him help
you.'*

As night approached, the contractions grew more regular. We had no accurate way
to time them. Den helped Theila to walk back and forth to speed the process. We
felt everything just like it were our own body. Despite the pain, we thanked God
that we could experience our child's birth.

Den kept Theila hydrated as sweat drenched her. Because of lack of food and
sleep, he quivered. He dozed off.

At three-to-dawn, a cougar snarled at the entrance of the cave. Denrick started
and drew a dagger. Theila whimpered in pain.

Denrick clapped his hand on his thigh and made as much noise as possible, but
the cat did not care. As it wound up for a pounce, Den charged it. His dagger
embedded in its left shoulder and it swiped Den with its right claw. When Den
recovered, he reached for the cat's face and dug his thumbs into its eyes. It
howled and clamped its jaws on his left arm. Den punched it in the jaw, then
grasped for the knife.

We longed to help him, but without a body, we could not. As the cat mangled his
arm, Den gripped the knife and plunged it into the cat's heart. The cat released
his arm and thrashed for a few minutes.

Theila's contractions grew more regular, but Den did not stir.

*'I know you're in pain,'* Nyla thought. *'But you must help Den. You are a
spider, so you have a special kind of venom for your husband. It will help him
to heal.'*

*'It hurts too much,'* Theila panicked.

*'We feel your pain too,'* WoeNyl said. *'But you need Den's help or the baby
might not survive.'*

*'I'm scared of him,'* Theila replied.

*'Ella,'* WoeNyl called to her mind. *'We know that we promised not to use our
power, but we will gladly take Theila's fear of Denrick if it means she will
help him.'*

*'Someone created the fear in her mind,'* Ella thought. *'I do not know how to
remove it and you may not steal it.'*

*'Isn't there anything you can do?'* Woethief asked. *'Can you redirect the fear
toward us?'*

*'I will try,'* Ella thought.

We felt Theila's fear of Den morph into fear of WoeNyl. Theila stumbled over to
her husband and bit him. He woke up. Theila hastily wrapped his arm in silk, but
the bleeding did not slow.

*'You may heal him, just once,'* Ella thought.

Nyla drew sweat from Theila to give us water for healing Denrick's arm. Skin
grew back immediately.

For the next three hours, Theila pushed. Pain overwhelmed us. We also sensed
Denrick's emotions, since they were so strong.

Finally, our beautiful child emerged. Den held her in his mighty arms. The same
hands that vanquished a cougar now cradled an infant. Crystal loudly announced
her presence to the world.

We felt the joy of parenthood for ourself and felt Den's fatherly pride and
Theila's motherly joy.

Since Theila feared us, we dared not spoil the happy moment by speaking to her.
Crystal's power emanated from her as she cast gray light throughout the cave.

After a week, help came for us. Four and men from Greenplains pulled a cart for
Theila and Crystal.

We entered Greenplains, which lived up to its name. Over time, Theila stopped
talking to our minds and our power waned. Nevertheless, we were content. Our
family was safe.
