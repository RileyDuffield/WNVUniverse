<!--

SPDX-FileCopyrightText: 2023 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Desmond

After the Cambrian War, Desmond Alish will marry Hard. Their marriage was
unplanned.

A group of villagers kidnapped Desmond and Hard. To keep herself from being
married to a stranger, she asked Hard to claim her. Hard barely won a fist-fight
with the villager and claimed her as his wife. Since the village held weddings
once a month, they forced Desmond and Hard to drink a love-potion and marry.

After a few days, Desmond and Hard transformed into beasts of burden, to help
with a canal digging project. Despite the extra-natural poison, they discovered
a way to gain their freedom and be restored to humanity. Their child was born,
looking like one of the beasts of burden.

After they settled on a hospitable planet, in a frontier community, they became
farmers. Hard, longing for adventure, became the sheriff.

Desmond was jealous and thought Hard was still in love with WoeNylVal, so she
forced WoeNylVal to leave them. She resented Hard's desire for adventure and
danger.

Despite Hard's sacrificial love and good parenting, Desmond felt insecure, since
she was plain. She took increasingly drastic measures to increase her beauty,
including drinking Strongwater and buying beauty enhancements from a
bottle-vendor.

Ruby wanted to help Desmond realize that Hard loved Desmond for her mind, so
Ruby turned Desmond into a hag. Any woman Desmond neared would permanently
become more beautiful than Desmond. After learning her lesson, Ruby planned to
turn Desmond into a wish-worthy.

Rather than submit to the discipline, Desmond used her ability to channel
WoeNylVal's power to turn herself into a copy of Nyla. Despite this change,
other women were still more beautiful than her, so Desmond also turned them into
copies of Nyla (more beautiful copies).

She used WoeNylVal's powers to send pain or sadness from her children to
WoeNylVal instead of addressing the root causes of the problems.

With her newfound power, Desmond turned herself and Hard into silkwings. Hard
mourned for the change because it would mean that they would outlive their
children. Desmond turned their children into silkwings too.

Because she changed the forms of every female in the community, Desmond was
accused of being a witch and Hard was ordered to arrest her. He asked her to go
to jail quietly, then immediately resigned as Sheriff.

His rival, who had tried to be elected as Sheriff before, took his place.

While both parents were gone, bandits razed their farm-house and kidnapped their
oldest son. They thought his beastly appearance would fetch a high price if they
sold him to the circus.

Desmond was sentenced to banishment and was transferred off-world in a
spaceship. She took her children with her.

Hard stayed behind, to rescue their son. Only after living without him did
Desmond realize how much her husband loved her.

