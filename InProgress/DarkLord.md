<!--

SPDX-FileCopyrightText: 2022 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Dark Lord

# The Prophecy

When hope's quite lost and war drags on

The Dark King's shadow lingers

Ere long shall rise the Hero's dawn

Scepter wrenched from fingers


A beast of West-North he shall slay

And raise an army mighty

The tyrant, he shall rue the day

He chose not flight, but fighting


The Fairest Maid shall love the man

Whose hosts shall right the wrongs

The darkness wrought upon or land
Rejoice, delivered throngs!

# Impertinance

The guards eyed each other nervously. They pitied the messenger, who had shared the news. He was sent to the worst outpost in the kingdom as a punishment.

An uninformed taskmaster marched new slaves into the throne room. The timing was terrible. The Magnificent did not want to inspect new slaves at a time like this.

"What am I to do?" the Magnificent roared. "Is there a man brave enough to defy the Prophecy?"

He swung his scepter wildly and paced on the dais. His black leather armor masked his aging body.

"Sire," Baron Trask said. "We are still analyzing reports from our spies. We need more information on the enemy's capabilities."

"He killed six beasts of WestNorth just like the Prophecy said," the Magnificent fumed. "We must kill him while we have the chance to stop the Prophecy's fulfillment. I will not let him kill me."

"We're considering our options for a strike, but he may have the Shield of Invincibility already," Baron Trask said.

"We must destroy him before he hurts me. It's the only way to stop the Prophecy," the Magnificent replied.

"You could stop being the Lord of Evil," a slave-girl of nineteen said.

"What did you say?" the Magnificent asked.

"You could stop being the Lord of Evil," the girl held up her chin and proclaimed. "The Hero cannot fulfill the Prophecy if there is no Lord of Evil."

A guard slapped her.

"Striking me will not change what I say," she said through a mouthful of blood.

"Come here," the Magnificent pointed his scepter at the girl.

She approached and knelt on one knee.

"Who are you that you dare speak to me as a peer?" the Magnificent asked.

"I am Loathing, daughter of Count Kneer," she said. "He was an ambassador before you kidnapped him and put him to work in your quarry."

"So, you speak to me as one who also has noble blood?" the Magnificent asked.

"I speak to you as someone who has nothing to lose," she replied.

Loathing was adopted, so she possessed no noble blood, but had a noble heart.

"Do you know what I do to insolent wenches?" he held up a necklace, with a bone encased in amber. "This is the finger of my sister. I killed her when she defied me."

"If you kill me for speaking, you will prove that you are the Lord of Evil and the Prophecy will be fulfilled."

"You are clever," he said.

"My father trained me in the art of statecraft. I know the way your enemies think because I am of their people. I will swear allegiance to you if you allow me to help you defeat the Prophecy." Loathing bowed her head.

The Magnificent whispered to an attendant, who led the girl away.

As Loathing traversed the cold, stone corridors, the attendant spoke to her. "You are to wear his sister's clothes. She died for her treason 35 years ago. You will be sworn into our lord's service."

"Why is our lord accepting my offer?" Loathing asked.

"The Prophecy must be stopped by any means necessary."

"I will aid our lord in stopping it," Loathing said.

As maidservants bathed her and helped her don black armor, she pondered her next move. She thought her outburst would bring the sweet relief of death. She didn't expect to be heeded.

***

In the Throne Room, she strode to her master, flanked by six guards. With her fiery eyes and upright bearing, she fit her armor well.

At the foot of the dais, she knelt and bowed her head. After a short speech, the Magnificent placed a crown of black feathers on her head, to mark her as the Chief Scribe.

The crowd saluted, but did not cheer. Loathing sat beside the Baron, who fumed with jealousy. She listened to the business of the court and took notes with her quill.

At supper, the Magnificent ate with her alone. His dumb guards  stared daggers at her.

"Sire," Loathing said. "Permission to speak freely."

"Granted," the Magnificent replied. "You may always speak freely."

"Why did you not kill me?" Loathing asked.

"Two reasons," the Magnificent chewed his steak deliberately. "You may be useful to me. No other slave has an air of nobility like you. Those in my court were instructed to forget where you come from. A loyal noble is hard to find, so I'll create one."

Despite her hunger, Loathing ate slowly, with manners. "What is the second reason?" she asked.

"I know in my heart that you are right. I am weary of violence and vice," he said. "I must consult the Voices, but perhaps they will allow me a respite for long enough to escape the Prophecy."

"There is more to say," Loathing said. "What is the third reason?"

"You look like my sister. She was not disloyal, as my court believes, but the Voices required me to sacrifice what I loved most."

"I mean to be forward," Loathing said. "If you wish to thwart the Prophecy, you must heed my voice instead."

"The Voices have led me thus far," he said. "You cannot replace them in a day."

"The Voices have led you to the brink of ruin. Consulting spirits is the wrong way to gain counsel," the tattered tapestries and scratched dishes emphasized her words.

"Enough," the Magnificent slammed his fist on the table. "Return to your quarters."

"Yes, sire," Loathing saluted. Her chestnut hair swayed as she sauntered off.

***

As the Magnificent completed the ritual to summon the Voices, Loathing prayed at the foot of her new bed.

"God, please frustrate his communion with the Voices. Please break the spell of evil that holds my master's heart captive."

She prayed into the night, until her weak body collapsed.

***

In the morning, her servant, a girl of thirteen, shined her musty armor. The girl eyed Loathing's breakfast.

"Sit, eat," Loathing offered.

"I cannot," the girl replied.

"I can tell you are faint," Loathing stood and pulled out her chair. "There is more than I can eat."

The girl, named Spite, nibbled an orange.

"I must know everything about the castle," Loathing said.

Spite told her that food and metal were short, guards wanted higher pay, and the Voices were more cruel than usual.

Loathing donned her armor and crown. Her full stomach improved her mood, but she feared she had angered the Magnificent too much.

As her guard escorted her to the Throne Room, he tripped often. His one, good eye was not enough to navigate the dim corridors.

When she entered the Great Hall, the Magnificent pointed his scepter at Loathing.

"Go to my private hall, now," he rasped.

"Yes, sire," Loathing saluted and sped to the room, where they had supped the night before.

When Baron Trask and the Magnificent joined her, she swallowed the lump in her throat. The Magnificent's pale skin and bloodshot eyes intensified his fellness.

"What did you do?" he roared.

"I retired, slept, awoke, then ate," Loathing replied. Her gaze locked with her ruler's.

"You did something, you witch," her ruler shoved her against the wall and raised his scepter. "They didn't come. I gave my blood to them, but still they didn't come."

"Who, sire?" Loathing asked.

"The Voices," he wailed. "I needed their wisdom."

Loathing shuddered, but chose to tell the truth.

"I prayed that they would be stopped," Loathing said. "My God can rescue this land from its enchantment."

"How dare you?" her master flew into a rage, then teetered. As he fainted, Loathing caught him.

"Fetch a healer," she ordered the Baron. "His bloodletting has made him ill."

Baron Trask opened the door and barked orders to a soldier. A few minutes later, the soldier reported that the only healer had died two days ago from a nasty fall.

"I will care for the Magnificent," Loathing said.

She and her guard carried their master to his private chamber and set him on a couch. When she removed his charm-covered grieves, fresh wounds on his wrists greeted her.

Bile rose in her throat as she hated the Voices, who demanded the blood of an old man. Her Savior had spilled his blood for her; she didn't need to cut herself to please him.

After she bound his wounds, the Magnificent awoke.

"What happened?" he asked.

"You lost too much blood. You need rest, sire," Loathing replied.

"My court expects me," he said. "I must rise and not show weakness."

"Sire, please rest," Loathing said. "Your body is spent."

"I see your plan. You'll take my throne in my weakened state," he rose, but tumbled to the floor.

As he clutched his bruised shoulder, Loathing helped him onto the couch.

"Sire," she said. "I will not leave your side until you are recovered. I have no designs on your throne."

***

During the night, fever set into his infected wounds. Loathing begged God to heal him. In his fever-dreams, he muttered about the Voices and shrieked in agony.

As the weak drew to a close, the Magnificent neared death.

"You can't let me die," he clutched Loathing's hand. "I'll be punished for what I've done."

"There is hope in my Savior," Loathing said. She wiped his forehead with a damp cloth.

"Your prayer was powerful enough to stop the Voices from coming," he said. "Your prayer can heal me."

"My God can heal you if he chooses, but your soul is of greater concern."

"Pray for me," he begged.

"God," she took her master's hand. "Please show your power and mercy by healing my master. Please save his soul and break the spell of the Voices."

Her master fell into a peaceful sleep.

***

As she dozed on another couch, footsteps sounded in the room. The guards stood still, as if nothing happened. Their backs were turned to the intruder.

When the assassin plunged his dagger toward the Magnificent, Loathing threw herself over him. Her armor turned the blade: mostly.

She screamed as she clutched her bleeding side. The guards refused to help.

To save her master, she grabbed the attacker's wrist. Before his dagger could touch her throat, the scepter gazed his forehead. The dagger clattered to the floor. The magnificent poised for another strike, but the attacker and crooked guards fled.

When loyal guards came, their torches revealed the design on the dagger. The Baron had tried to kill his lord.

Because of blood-loss, Loathing went into shock. A competent healer had arrived that night and cared for her and her master.

***

In the morning, Loathing opened her eyes to see her master armored and walking.

"Your prayer worked," he said.

"My God is kind," Loathing replied.

"You have remarkable courage," he said. "Why did you save me?"

"My father made me promise to bring peace between his land and ours. I need your help to do that," Loathing replied.

"Whose side are you on?" the Magnificent asked.

"The side of peace," she held her hand over her wound. "140 years is too long for a war."

"Guards, leave us," the Magnificent commanded.

"Now, we are alone," he said. "Take this and I will turn my back," he pressed his scepter into her hand. "If you strike me with my own scepter, you will gain the throne."

He sat before her, within arms reach.

"I am your servant, sire," she said. "I cannot harm you."

She set the scepter beside her. The Magnificent waited for a few minutes before turning to her.

"I put my life in your hands and you did not take it?" he wondered.

"Life is a precious thing," she said. "You saved my life last night. How could I spurn that gift?"

"You truly are loyal," he said.

"Yes, sire," she said. "Unless you demand something against God's law, I will obey you."

"I believe you," he said.

***

In the afternoon, subjects presented their cases to the Magnificent in a vain search for justice. Their ruler sided with the strong, against the innocent. Their respective gloating and wailing broke Loathing's heart.

"You may decide the next case," the Magnificent told Loathing.

A peasant was caught stealing an orange and brought for punishment: branding his eyes. Through his tears, he told of his family, who needed his support.

"What was the orange worth?" Loathing asked.

"A halfpence," the bailiff said.

"Your theft is unacceptable," Loathing announced. "As payment for your crime..."

"Please show mercy," the thief interrupted.

"Silence," Loathing shouted. "You must pay one pence to the merchant and a halfpence to the state. If you cannot, you must work one day cleaning the street."

The thief's jaw dropped. He muttered thanks, but Loathing ordered the bailiff to bring in the next case immediately.

The court murmured about her unusual sentencing. Who did she think she was?

Guards shoved a disheveled boy of 14 in at spear-point. Bruises and cuts marked his body. A gray stone was fused to him above his breastbone.

"This is the son of the former baron, Trask," the bailiff announced. "His father fled in cowardice, so his son will be punished."

"You may decide this case as well," the Magnificent said.

Loathing took the dagger, which had wounded her, and approached the boy.

"Your father is an evil man, who ruined his family name," she announced.

The boy glared at her with hard eyes.

"It is your responsibility to restore the honor of the House of Trask. To give you that opportunity, I restore to you all the titles and lands of your father. Serve our lord well, Baron Trask," she said and handed him the dagger. She led him by the hand to the seat of his father.

The new baron saluted the Magnificent.

"I will redeem my family name," the boy declared. "I am your loyal servant, my lord."

"You are strange, Scribe Loathing," the Magnificent said. "Baron Trask, have my healer attend your wounds. You will swear your allegiance to me tonight."

"Yes, sire," the boy saluted and limped out of the hall.

At supper, the Magnificent asked for an explanation of the rulings Loathing made.

"The peasant will remain able to work, since we did not blind him," Loathing said. "I gave him a punishment that matched his crime."

"And what about Trask?" her master asked.

"I kept the House of Trask loyal to you. Now, they have no reason to seek revenge. The boy was innocent and I believe he will be a loyal asset."

"You seek to win favor with the populace," the Magnificent said.

"Yes, I aim to increase my influence. As I rise, you will rise too. I aim to be the shoulders you stand on," she said.

"You are ambitious," he said. "Ambition is dangerous."

"Loyal ambition is not," she replied. "I need power to bring peace."

"I will allow you to use your methods for now, but if I sense disloyalty, I will kill you," he warned.

"Then my life is safe," Loathing sipped a glass of soft-cider.

***

Over the course of the next two weeks, Loathing decided all cases that came to the throne room. She codified new laws, so the lower judges could consistently apply them.

The new Baron Trask proved loyal and used ever opportunity to gain the favor of his ruler.

Baron Trask strove to master his duties heading the military and police. His lifelong training mitigated his youth. At Loathing's advice, he surrounded himself with wise advisors.

His lieutenant made may day-to-day decisions as Trask gained competence.

A soldier entered Trask's quarters with a message. His eyes focused on his sandals.

"What is the news, soldier?" Trask asked.

"The Hero is staging troops on our border. We estimate that he has seventy-thousand men already."

"How confident are you?" Trask asked.

"Highly confident," the soldier said. "We counted their regimental standards."

"We must tell the Magnificent," Trask said. "Follow me."

In their war room, the soldier briefed the Magnificent on the threat.

"We have forty-thousand men, who are well fortified," the Lieutenant-General said. "We can hold the invaders off."

Another soldier burst into the war room and saluted sharply.

"Baron Trask," he said. "Permission to speak privately."

"Denied," the Magnificent said. "We will all hear it."

"The Hero united the northern and western territories. One-hundred-thousand men are a week from our northern border."

The soldier trembled.

"Why am I just now hearing of this?" the Magnificent asked.

The soldier, who brought the bad news, did not answer.

"Speak, man," Loathing said. "You will not be punished for telling the truth."

"The former Baron ordered us to withhold the information. He did not tell us why," the messenger said.

The Lieutenant-General fidgeted in his chair. His eyes darted around the room.

"Did you know about this?" the Magnificent asked Baron Trask.

"No, sire," Baron Trask said. "Until my swearing in, I was in training every day."

"Did you know?" the Magnificent pointed to the Lieutenant-General, who nodded at his guards.

Swords flew from their scabbards and guards rushed the Magnificent. Baron Trask and Loathing stepped in front of their master to shield him.

Trask's swiftness could not compensate for his lack of strength. The boy's sword clattered to the floor as his Lieutenant-General moved on him.

Loathing's guard plunged his spear into the traitor. Throughout the palace, guards from different cantons skirmished.

"We must get you to safety," Loathing said.

"No," the Magnificent replied. "I must not show cowardice now."

Loathing's guard escorted her to her quarters. Spite rushed into the guard's arms.

"Daddy, what's going on?" she asked as clashes of steel echoed down the hallway.

"There's a coup," he said. "Bad men want to hurt our lord."

He held his spear toward the barred door.

"Let us pray," Loathing took Spite's hands. She implored God to protect her master and the Baron. Spite trembled in her arms.

***

In the throne room, loyal guards formed a wall of spears around their lord. Trask dragged the body of his advisor to a balcony, which overlooked the courtyard.

"Your leader is dead," he yelled to the rebels. "Lay down your arms."

"Why should we?" a man from the Lieutenant-General's canton asked. "We're as good as dead now."

"Any man who has not been stained by the blood of those loyal to the Magnificent will be granted his life. You will be discharged from service and branded."

Loyal guards surrounded guards from the House of Trask and the Lieutenant-General's canton.

"What is your answer?" Trask asked. "Will you surrender or die?"

Some men threw their hands up, but their troublemaking companions killed them. The disloyal guards were killed to a man, with their weapons clenched in their lifeless fists.

A bloodied soldier of Trask staggered into the courtyard. He clutched his eyes and tripped over a body.

"Help," he yelled. "The Magnificent is in danger. Trask's loyal soldiers carried the man to the balcony, up a flight of stairs."

"Are you loyal?" the wounded soldier asked. "I can't see."

A gash across his face had destroyed his eyes.

"Loyal to whom?" Trask asked.

"Our Magnificent," the guard said. "My squad mutinied. They tried to stop me from warning my lord."

"I am loyal," Trask said.

"Warn the Magnificent," the guard begged.

"He is safe," Trask said. "Get all the friendly wounded into the great hall."

Trask's men obeyed. Thirty-two dead and seven wounded were the result of the coup. With the Lieutenant-General dead, no one dared to rebel again.

When Loathing and Spite came to help the wounded, Trask asked for water. He <!--Show Trask healing soldiers, using the shard.-->

The blinded soldier's story was verified by a man from a loyal canton.

***

As dusk descended, the council of war reconvened.

"How do you propose that we meet the enemy?" the Magnificent asked.

"This decision is difficult," Loathing said. "We must seek wisdom from a higher source."

"I will fetch my vision pool, so we can ask the voices," the Magnificent said.

"Sire," Loathing said. "The Voices led you to bloodshed. Your forefather started this war because the Voices told him to. We are short on food, our troops have low morale, and we are outnumbered. Only my God can help."

"Scribe Loathing is right," Trask said. "The Voices cannot help us."

Loathing held out her hands and Trask took one. Despite his misgivings, the Magnificent took her other hand. A strange longing filled him as he noticed her beauty. He must not let a woman distract him.

"God," Trask prayed. "We need wisdom. Our ways have been evil. Give us the ability to right them, so the Prophecy is not fulfilled. Guide our people to you."

"Thwart the Voices and the mutineers. Forgive our sins, lest we be destroyed for our wickedness. Do not let our enemies destroy us; bring peace between us," Loathing prayed.

"You prayed against them again," the Magnificent said. "The voices are the strength of our army. We cannot win without them."

"We cannot win. We must sue for peace," Loathing said.

"But my fathers trusted me to finish the war," the Magnificent replied. As he gustured with his arms, his charms jingled.

"We will finish it," Trask said. "I will raise as many troops as possible. We will prepare for a siege."

"We can't fight our way out," Loathing said. "We must try to reconcile."

"What do you propose to offer them?" the Magnificent said.

"We will return the territory we annexed and repatriate the slaves and prisoners of war. We will give half the gold in our treasury as restitution," fear shone in the scribe's eyes as she gave unwelcome advice.

"This is outrageous," the Magnificent said.

"We are the villains," Loathing replied. "To break the Prophecy, you must not be the lord of evil anymore."

"Withdrawing from their territory will allow us to consolidate our forces. Sending back their slaves will ease our food burden," Trask said.

"We will not surrender," the Magnificent declared. "But you must buy time."

"Allow me to return the City of Purple as a token of our sincerity. We will ask for a six-month armistice to buy time to prepare our defense," Loathing said.

"So be it," the Magnificent said. "Trask, give the order. All able-bodied men must report to their canton officers. We will not be defeated."

Everyone worked through the night to prepare for negotiations and war. In the morning, news came that skirmishes broke out along the border.

Trask prepared his chariot to go to the front. As he rode at the head of a parade, Spite watched him with admiration. He dismounted on the castle steps. All the soldiers behind him saluted the Magnificent.

After a speech from the Magnificent colling his men to valor in the name of the Voices, the war council rested in the private hall.

"You are a woman of remarkable beauty," the Magnificent said to Loathing. "But your tongue is sharp. Do not insult our enemy."

"Sire," Loathing said. "I can change my projected personality as needed. I know you are a hard man, so I pretended to be brash. I will be whatever is most persuasive to the Hero."

"Our future depends on you, dear," drowsiness caused the Magnificent to drop his guard.

Loathing noticed the word "dear".

*'Is "our future" about us or the kingdom?'* she wondered. She sensed that her king was falling in love with her.

"I will not fail you, sire," she said. "Please ask the people to pray for me."

"You must go now," the Magnificent fled the room to hid his blushing face.

Trask and Loathing descended to the entrance. On the steps, Spite handed Loathing a pack of supplies and kissed her hand.

"Be safe, mistress," Spite said. "I will pray for you."

Spite turned to Baron Trask and bowed.

"Baron, I see that no woman has given her colors to you. May I?" she pulled a rudely-made pendant from her apron.

"Do you trust her?" Trask asked Loathing.

"Yes," Loathing said. "She has served me well."

"I will wear your colors," Trask announced.

The girl tied her pendant to his should pauldron.

"Ask and my tutor will aid you. Study hard and become a lady worthy of the name of Trask," the young general said.

Trask, Loathing, and her guard sped toward the front in the chariot. Loathing marveled that the young man accepted a peasant's advance.

As they rode, Loathing asked why he accepted the colors.

"My mother tried to murder my father, so she could gain power for her lover. I want a wife I can trust. Beauty, wealth, and power are less valuable than loyalty."

"You trust me," Loathing said.

"We both follow the Way," he said. "So I trust you."

"Why did you offer a tutor to my daughter, General?" the guard asked.

"I aim to turn her into a noble, so when I marry her, in a few years, she will be ready," Trask said.

"Thank you, my lord," the guard said. "I did not know she would ask, but I am glad you accepted."

"I will honor her," Trask said. "If I survive this war, I will make her my wife. If I dide, I will ensure that she is cared for as my betrothed-widow."

Loathing and Trask curled up on opposite sides of the chariot, while the guard drove. Despite their fears, sleep visited them.

***

Near the City of Purple, evidences of fighting manifested. Walking wounded staggered away from the front, as did newly captured prisoners of war. An escort of fifty horses accompanied the chariot.

In a ruined tavern, which served as headquarters, Trask's men informed him of the city's defenses and the enemy's position.

To the soldiers' dismay, Trask told them that they were to withdraw over the next two days.

"We've lost a dozen officers in this city, just this year. How can we throw away their blood?" a knight asked.

"We follow the Magnificent's orders," Trask said. "You may read them for yourself." He placed a stamped parchment on the table. It bore wax seals from the Magnificent, Trask, and Loathing.

"Who's she?" another knight asked.

"I am Chief Scribe Loathing," she said. "I am here to meet with the enemy. This withdrawal from the city is necessary for his majesty's strategic goals. Do your part and help to consolidate our defense."

"It's hard for a soldier to retreat," the first knight said.

"I hear that soldiers excel in doing hard things," Loathing replied.

Trask outlined a plan to withdraw from the city in sections, which they indicated in a sandbox. After all men were accounted for, they would burn the bridge over the Brownfish River. No civilians were to be hurt and any food that could not be carried was to be left intact.

"This is outrageous," a knight roared. "The enemy will use our foodstores against us. We must burn the city as we leave."

"The citizens of the enemy are not to be harmed. We are leaving the food to ensure a stronger negotiating position," Loathing said.

"It would be stronger if they were weaker," the knight said. "Who let a woman and an upstart run the military?"

"Soldier," Trask said calmly. "We represent the Magnificent. We act on his orders. Show the loyalty you are known for and your obedience will be rewarded."

"Yes, General," the knight bowed his head in shame.

"You," Trask pointed to a cavalry officer, named Bryce. "What is your question?"

The officer looked startled to be called on.

"What will happen to the civilians who want to leave with us?"

"They may come if an officer vouches for them. They must be unarmed. I don't want an attack on our rear," Trask said.

"Thank you, sir," Captain Bryce replied.

"I will remind you that only willing civilians may come. No slaves will be taken across," Trask said.

"Sir," Captain Bryce began. "My concubine and her mother are my slaves. I love them." His cheek twitched.

"If you love them, free them." Trask said. "You have leave for one hour. bring them to the bridge if they will come. A priest will perform the marriage, then you will oversee the civilians' evacuation."

Runners spread the plan of the withdrawal across the city. As noon passed, an orderly retreat took place in silence. Few civilians followed.

Loathing and her guard, who had shed his weapons, approached the enemy lines with a conspicuous white flag.

"What is your business?" a burly officer shouted.

"I am an ambassador of the Magnificent. I will parley with the Hero," Loathing shouted.

Six spearmen surrounded them and patted down the emissaries. Loathing hated to be touched by a sweaty soldier, who enjoyed his task too much.

"How do we know you're an actual emissary?" the officer asked.

"I wear the armor of the chief scribe and I carry a royal signet," she replied.

"This is mighty irregular," the officer said. "There hasn't been an emissary in 70 years."

"There is today," Loathing said. "Do your duty and convey me to your Hero."

The officer ordered his spearmen to blindfold the "spies" and lead them to the Hero. Despite his rough appearance, the soldier, who guided Loathing by the hand, was gentle. He ensured that she never tripped over a loose stone.

When the soldiers had convinced royal guards to allow the emissaries to wait for the Hero's return, Loathing thanked the kind soldier and learned that his name was Truehaft.

As they waited, passing soldiers stared at Loathing and catcalled. She despised them.

After three hours of stoic silence, the Hero greeted Loathing and her guard.

"Why have you come?" he asked.

Loathing peered into his emotionless eyes. "The Magnificent sent us to request an armistice of six months."

"You are quick to speak," the Hero said.

"As a sign of good faith, we are withdrawing from the City of Purple. The city will remain intact, save for the bridge over the Brownfish."

"I have not accepted the armistice," the Hero said. "It seems more like you are bluffing to cover a shameful retreat."

"What gain would you receive from wasting the blood of your nation instead of using an armistice to refortify you borders?" Loathing asked.

"You are one of us. Your hair and skin make that obvious," the Hero said. "Why do you wear the uniform of our enemy."

"I am the sworn friend and servant of the Magnificent. My skin is of no importance," Loathing replied. Her gaze never broke from the Hero's eyes.

"You are a traitor to our people if you stay with him," the Hero said.

"I am loyal to the Magnificent," Loathing replied.

"You are a woman of remarkable beauty," the Hero traced her features with his eyes. "Surely, you could mary any noble you wanted to, even a legendary one. You will be welcome in this land."

*'How dare he insult my honor?'* Loathing thought.

"I am loyal to the Magnificent. Any boons Providence has granted me are for the service of my lord," she said.

"We will speak more at supper," the Hero replied. "My servants will provide fresh garments for you."

***

In her tent, servants prepared Loathing's hair, filed her nails, and smeared pigments on her face. Despite her protests, they forced her to change into a revealing dress. To avoid spoiling the negotiations, she bore the indignity without complaint.

When her guard saw her, he frowned. He pressed a stone into her hand. To her relief, a gray cloud hid her skin, to preserve her innocence.

Despite the urgency of her mission, supper was served at sundown. Soldiers gawked as she was escorted past their campfires. Her stomach growled.

The Hero frowned at the fog, which protected Loathing. The meal was served on silver dishes, which reflected and refracted Loathing's face.

"I trust you are rested," the Hero said. "You look magnificent."

"That title is reserved for my master. I am alert to discuss my mission," Loathing said.

"Tell me a bit about yourself," the Hero said as he carved a ham.

"I am an emissary for the Magnificent. I do his bidding," she said.

"What was it like in the quarries?" the Hero probed.

*'How does he know about that?'* Loathing wondered.

"My lord's quarries produce the stone for our houses and cities. We keep meticulous records of our outputs and cut stones perfectly to their specifications," she said with the straight bearing of confidence.

"Your father oversaw a quarry, did he not?" the Hero plopped meat onto her plate.

"Yes, his management doubled the quarry's productivity. I am proud to be his daughter," Loathing smiled at the memory of her father's love of the ledger.

She ate the food deliberately, but did not touch the wine.

The Hero steered their conversation away from her mission and exasperated her with his depth of knowledge of her life. He must have an extensive spy network.

"We will have an easier time discussing business in my tent," the Hero said. "I am sure your guard is tired. He may turn in."

"I am not the kind of woman you think I am," Loathing said. "I will not go without my guard. My colors are my own."

"The Prophecy speaks of you," the Hero said.

Loathing had never heard the entire Prophecy, so the Hero recited it.

"You are the goddess of humble origin. I am the Hero. Your master is the one to fall," the Hero said. "It is inevitable that you will end up with me. Why not enjoy our love now?"

*'Because I will break the Prophecy,'* Loathing thought.

She spoke carefully, "I am a lady. Only the man who wears my colors may enjoy my companionship. If you will not treat, I request permission to return to my master."

"You will change your mind," he nodded to soldiers, who escorted her back to her tent.

"Scribe," her guard whispered. "With my new eye, I could see through the blindfold. I know the way back."

The gray fog kept the enemies from hearing their conversation.

"Take my colors to the Magnificent," she said. "The Prophecy said I would choose the victor. I betroth myself to the Magnificent, if he will have me."

She returned the stone to her guard. Wreathed in silent, gray fog, he carried her pennant to her master.

## Treachery at the Bridge

As the last troops crossed the Brownfish, Trask and Captain Bryce crossed their units off the list. Only about seventy men remained.

Suddenly, hundreds of enemies appeared in the rubble. Volleys of arrows cut off Trask's retreat. Panicking soldiers burned the wooden bridge, stranding their general. His men pressed their backs to stone walls to shelter from the enemy missiles.

"Take these to the capital, now," Trask gave his dagger, shard, a letter, and ring to Captain Bryce.

"What about you?" Bryce asked.

"I will manage," Trask said. "Go"

Bryce waded into the river, under enemy fire. An arrow penetrated his lower back and he collapsed into the water.

Red mixed in the water with burnt wood and flaming oil. Water rushed into his mouth.

*'May I rescue you?'* a woman's voice asked in his head.

*'Who are you?'* Bryce asked as he clawed to the surface.

*'I can heal you,'* she replied.

*'Yes,'* the man begged.

The arrow disappeared into the river and healing surged through Bryce's body. When he clambered up the opposite bank, smoke obscured him from the enemy. To his surprise, the letter was still dry.

"Thank you," he said to the woman. "Where are you?"

"I'm the stone shard. Please bring me to Spite," she said.

"What are you?" he wondered.

The woman explained her past life, before she transformed into stone. Her

