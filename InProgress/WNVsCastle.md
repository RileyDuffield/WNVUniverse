<!--

SPDX-FileCopyrightText: 2021 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# WNV's Castle

1. WNV finds a castle-lord about to execute a chandler and his wife because they have not paid their debts.
2. She uses a lighting pole to defend the couple.
3. She kills the lord when she strikes him on the head.
4. Because she was unarmored when she challenged him, he ordered his guards to not intervene, since he was honor-bound to fight alone.
5. Since she (an unarmored combatant) defeated the lord (an armored combatant), she inherited his title and serfdom when he died.
6. As they returned to the castle, with the body of the lord, the guards attested to the contest.
7. The next day, WNV was declared ruler of the serfdom.
8. In private, lord's wife, WNV profusely for saving her from her life with her cruel husband.
9. WNV forgave all the servants' debts and decreed that servants should be paid for their labor.
10. The lord's younger brother marched to her castle with his men.
11. Rather than fight and risk having her people die, she challenged the brother to single combat, to the death.
12. She defeated him but spared his life, since she bore no ill-will against him.
13. She made him her lieutenant to gain the people's good-will, since he was popular.
14. He married his brother's widow.
15. WNV issued other reforms like providing free literacy classes to all her subjects and improving the conditions in the dungeon.
16. When she was ordered to raise a levy, she refused because the war was unjust.
17. The king sent his men to crush her "rebellion".
