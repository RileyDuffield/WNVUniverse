<!--

SPDX-FileCopyrightText: 2020 Riley Duffield <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Operation Firestorm

![Operation Firestorm Cover](CoverArt/OperationFirestorm-Cover.png)

Cotton shook Cougar to wake him from his nap.

"Let me sleep, I'm on night watch," Cougar grumbled.

"Not anymore, there's a squad meeting."

Cougar pulled on his field jacket and buttoned the frayed fabric. He ignored the stubble on his chin. He hoped his CO would too.

"How can we have a squad meeting?" Cougar asked. "Half our squad is on an op."

"Captain Rein asked for us."

"Great."

Cougar and Cotton fell in with their fireteam and saluted Captain Rein, who did not return the salute.

"Highroad Squad was tasked with locating Colonel Long, an enemy officer in charge of an airship with a weapon capable of incinerating entire buildings at an unknown range.

"Last night, our lookouts spotted a large explosion. We believe the enemy airship crashed 50 kilometers to our northeast. Your task is to capture Colonel Long or recover any information about this weapon."

Rein shook his head. He doubted Highroad Squad. Everyone did.

"We can only spare Fireteam Clarice, so the four of you will need to avoid engagement as much as possible. The enemy airship carried between 40 and 80 crew. We do not know how many of them are still alive.

We expect a rescue party to arrive from the enemy FOB 30 kilometers east of the crash site, but the terrain is so difficult it might as well be 90 kilometers," he pointed to the locations on the map.

"Do not engage the enemy unless absolutely necessary.

Travel light. Make us less ashamed of Highroad Squad. Remember, we want Long alive. Leave in 15 minutes. Dismissed."

Rein stomped off in his polished boots. His pistol glistened on his belt next to a cigar box.

"This map's pretty old," Cougar said. "How are we supposed to operate with this shoddy intel?"

"We always find a way," Song, the team sharpshooter, chimed in.

"What did he mean 'make us less ashamed'? What did we do to him?" Hard asked.

"The same thing we always do, Hard, we play by stricter rules," Cotton replied. "Now kit up. Make sure your blades are sharp. We might need to kill quietly."

Fireteam Clarice packed up with five minutes to spare. They had little equipment. The kids from Fireteam Daphne saluted them and stammered encouragements. Fear widened their eyes. Their first raid was scheduled for tonight.

"Cheer up, lads," Song said. "You'll make it through this raid and prove to the rest of the army that you're the best fireteam."

"We need to move," Cotton grabbed Song.

The four men marched toward the crash. Birds didn't sing as they traveled into the forest. A perimeter guard made an obscene hand-gesture at Clarice as they passed. Song kept Hard from clobbering the guard.

"Why did they only send four of us?" Cougar asked. "What are we supposed to do when we meet the enemy?"

"They never send enough of us. I wish we could force Rein to come with us one of these days. He'd learn fast what it's like to go on these missions," Hard declared.

"Respect for officers," Song said.

"We know, Song," Hard grumbled.

"Only four of us are going because we can be fast and quiet," Cotton said. "No matter how much he puts us down, Captain Rein knows we can practically turn invisible in the woods."

"Do you ever think there is more to them sending us on such hard ops with no support?" Cougar asked. "I don't think Rein would cry if we didn't come back."

"Rein sends us on whatever missions the brass thinks are too risky for the army. He thinks we are expendable. There is an enemy FOB 30 kilometers east of the crash site. The brass can't risk a real battle. They are busy gearing up for something big." Cotton said.

The men continued in silence to conserve energy. They missed their leader. Sergeant Cotton was proficient, but Captain Twilight always knew how to inspire them.

At the end of their march, they felt like their feet would fall off. Dusk hovered over the wooded terrain. A natural stone bridge led over a 200 meter deep ravine to a ring of charred trees. The explosion happened nearby.

Fireteam Clarice rested in a thicket as Song surveyed the area through a pair of half-busted binoculars.

"How are we going to search that area?" Hard wondered. "The cover's all burned up."

"We'll wait for nightfall," Sergeant Cotton said.

"But, the rescue party could get here soon," Cougar said.

"Nah, the rain last night will make the mountain streams difficult for them to pass. They'll need to build bridges or take long detours. We have a little time before they show up," Song said.

"I'm parched," Hard said. "Sarge, how 'bout we find some water before we go in."

"Get us some too," Cotton handed over his canteen.

Privates Hard and Cougar crept out of the copse in search of a stream. They followed the mosquitoes to a promising spot.

"Hold up," Cougar grabbed Hard's arm. "That's a wail-badger hole. One cry from a badger and the whole forest will know we're here."

"We need the water. We don't have time to find another stream. The bank is too steep everywhere else." Hard said. "Fix bayonets."

The men crept toward the hole with blades ready. From behind a log, a badger lunged for Cougar. Cougar's bayonet missed. He slipped in the mud and face-planted.

As the badger swiped at Cougar's face, Hard blew its brains out with his revolver. The shot rang off the rocks on the other side of the stream.

"What do you think you're doing?" Cougar whispered.

"Saving your life," Hard replied.

"They know we're here now. You should have used your knife."

Hard popped his last spare round into his revolver. Cougar filled their canteens as quickly as possible. To conceal their presence, Hard mangled the Badger's face with his knife to make it look like claws killed it. He did a bad job.

When the privates slipped back into the copse, Cotton glared at them.

"What was that shot? Cougar, why are you bleeding?" Cotton asked.

"It's badger blood. Hard got trigger-happy," Cougar replied.

"Hard, if you can't learn to control your trigger, you'll get us all killed one of these days. Light and noise discipline are in effect. We move out in 15 minutes," Sergeant Cotton ordered.

Clouds hid the full moon. A light rain soaked the men. At the 50 meter-long land bridge, they paused behind a fallen tree. Nothing stirred past the bridge.

"That rock's pretty slick. We should set up a safety rope?" Song said.

"None of us have rope," Hard said. "We had to travel light, remember?"

"I have some," Song whispered. "But it's only 20 meters. We need 50 to cross the bridge."

"No one else has any," Cotton said. "Crawl across. I'll go first. Song you cover us and come last."

Cotton and Hard crossed without event. As Cougar crossed, he slipped. He caught the edge of the bridge with his hand, but his rifle plummeted into the ravine.

Hard raced across and yanked him back onto the bridge.

Once everyone made it across, Song pulled out his binoculars. As he scanned the burn zone, he spotted a flicker a half kilometer away.

"There's a fire over there," Song whispered. Cotton borrowed the binoculars.

"Skirt the burn zone. I want to know what's around us before we go into the open."

Clarice moved along the edges of the burnt trees. Occasionally, a shaft of moonlight filtered through the clouds.

Cougar tripped. When he picked himself up, he found twisted aluminum and strange fabric. After stuffing a fabric scrap in his pocket they moved on. The airship definitely crashed here.

Song scanned toward the flame again. With a temporary streak of moonlight, song spotted a head near the camp fire.

"We haven't seen anything else yet. Song, go investigate. Do not engage," Cotton ordered.

Song crept forward with his rifle on his back and a blackened knife in his hand. He disappeared in the open. Cotton wished he knew how to do that.

After 15 minutes, Song returned.

"There are seven men at the fire. Three are awake and the others are asleep. I couldn't tell if they were all armed. One was definitely missing a leg," Song said.

"Can we get around them?" Cotton asked.

"Yes. There's a rise to the north of them. We can move around them unseen." Song said.

As they crept around the edge of the campfire's light, Song discovered a body. Cougar, the team medic, examined it. With his fingers, Cougar read the officer's insignia, leftenant. The only interesting thing on the scorched body was a key. Cougar slipped it into his pocket.

150 meters away, a rectangular object jutted out from the ground at an odd angle. Clarice moved in while Song provided overwatch.

Inside the object, two voices yelled indistinct words. As they waited outside, Clarice realized the structure was part of the airship's cabin. Splintered wood jutted out from either end. Small dots of orange light smoldered in various places.

After circling the cabin, they found only one entrance. The cabin retained its shape surprisingly well. There was obviously much more to it once, but the core was intact. It must have been fireproofed.

Cotton ordered them to try the key on the door. It worked. After easing the door open, they waited.

A head popped out of the doorway, and they grabbed its owner. They muffled the man's yells, but he kicked Hard in the face. Hard fell unconscious.

After gagging and binding the man, they tied him to a tall stump. Hard woke up with a bad case of trigger-happiness. Cotton told him to watch the door while they interrogated the prisoner. By the prisoner's insignia, he was a corporal.

"We're about to ungag you. If you yell, you'll get it. If you tell us what we need to know, you'll live." Cotton held a knife to the corporal's throat.

They ungagged the corporal.

"Say fellas, what'd you have to do that for. I'm a reasonable man. I know how to make a bargain," he slurred. They smelled alcohol on his breath.

"Who are you?"

"Corporal Walsh, engineering. Say, if you're gonna ask me all these questions, the least you can do is give me a smoke."

"What happened here?"

"The big balloon went kablooey," Walsh said. "What do you think?"

"What was your mission?" Cotton asked.

"Say, I need a smoke. They wouldn't let us carry them because hydrogen goes bang and all."

"We don't have any."

"Why not?" Walsh demanded.

"Light discipline."

Walsh grimaced.

"Stupid officers ruin all the fun. They're always yapping at you and telling you you can't do nothin'. Well they got what's coming to them. Kablooey. Good riddance."

"How many of you were there?"

"Do I look like an accountant? There were at least two dozen but they went... Say do you have a nip for me?"

"No, we haven't. Where are the survivors?"

"Ain't no survivors, just us."

"He's wasted," Cougar shook his head. "Stop wasting your time."

Walsh twisted his hand free from the bonds and reached for his pocket. Cotton knifed his throat. When Cougar searched the body, he found a booklet and a clasp-knife in the pocket. Was he reaching for the paper or the knife?

"Hard, let's get the cabin now."

The three men prepared to raid the cabin.

One

Two

Three

They rushed into the cabin, knives and pistols ready. A single candle lit the interior. A boy held his hands over his head. No one else occupied the cabin.

"Hard, sketch this," Cotton ordered.

Hard dug in his waterproof satchel for a pad and pencil. He sketched each wall of the cabin as quickly as he could.

While Hard worked, the boy eyed a stack of papers on the floor. Cotton stooped to retrieve them, but the boy kicked the candle onto the pile. Flames licked the paper. Cotton stamped out the fire too late.

Cougar held his pistol steady on the boy.

"What were those papers?" Cougar asked.

"Corporal Walsh wanted to sell them to you, but I wouldn't let him."

Cotton flicked a pocket lighter and read the heading on a scrap of paper.

"Navigational charts," Cotton said.

"Where is Long?" Cougar asked.

"That's need to know," the boy's lips trembled.

"This kid's not going to tell us anything," Cotton said.

"I can make him talk," Hard slipped on a pair of knuckles.

"Not that way, Hard. We aren't like the rest of them. We need to move."

Hard shrugged and slipped the knuckles into his pockets. Cotton poured a drop of liquid from a vial onto a rag.

"That took guts kid," Cotton said as he placed the rag over the kid's face. "You'll wake up in two days without memories of this week. Considering the crash you went through, this is a blessing."

The boy blacked out.

Clarice scoured the cabin, but found nothing useful. The kid did his job well. They gave him props for that. Too bad he wasn't part of Highroad. While they had a chance to use light, they investigated the booklet. It contained schematics for the ship.

Rain pummeled them as they exited the wreckage. Poor Song. He must be halfway to hypothermia by now. Clarice crawled through the mud to Song.

"What'd you find?" Song asked.

"Some diagrams," Cotton said. "A kid burned the nav charts."

"While you were out of the rain," Song said. "I saw some light up in that rock outcropping."

"What about the campfire?"

"Nothing's changed. They don't know we're here."

"We'll check out the rocks and find you a new position. It's too dark for you to make long shots anyway."

Clarice maneuvered to the base of the outcropping. Cougar climbed the slick rock and peeked over top of the ledge with his pistol drawn.

"Blanket," a voice moaned.

An enemy airman lay two meters away under a makeshift tent. His blanket rested on the ground next to his stretcher. The wounded man met Cougar's gaze.

Cougar signaled for his team to stay behind and crested the ledge. Singed trees blocked their view of him. He needed the enemy to be quiet. As he placed the blanket on the airman, he noticed a dirty bandage wrapped around the man's arm. The arm was sure to get infected that way.

Cougar hated his compulsion to help people. It got him into trouble. Cotton would be mad, but Cougar was a medic first, soldier second. He hated killing.

He sanitized his hands, removed the scrap of a shirt from the man's arm, and applied a salve. As he worked, a rifle barrel poked into the back of his head.

"Who are you?" an enemy demanded, hidden from Fireteam Clarice by the trees.

"He needed a fresh bandage."

Cougar finished wrapping the arm, while the man confiscated his pistol and one of his knives.

"Who are you?" the man repeated.

"I'm a medic. I came after I heard about the crash."

"We'll see what Alish has to say."

The man forced Cougar toward the center of the camp, away from Fireteam Clarice, to an overhang. A sergeant scrutinized him. Her hair clung to her face in soggy strands.

"I found him bandaging one of our men. He won't tell me who he is."

"Return his weapon," the sergeant ordered.

"But, I don't recognize his uniform. He could be an enemy."

"That's an order, Carlton," Alish said.

"Yes ma'am."

Cougar holstered his pistol.

"My name is Sergeant Alish," she held out her hand. "We need medics."

Cougar shook her hand.

"I know you are an enemy," Alish said. "But I recognize your squad patch. I will allow you to keep your weapons if you help treat the wounded. I can't afford a firefight right now."

"Do you have any medics?" Cougar asked.

"Only Private Archie, but he hasn't slept since the crash."

"Why do you trust me?" Cougar asked.

"I am desperate. There are still wounded out in the open. We are lightly armed, sleep-deprived, and our chain-of-command has collapsed. And I know your reputation."

"If you don't mind my asking, why haven't the wounded been recovered."

"The explosion was very violent. It was followed by an intense fire, then rain. I was unconscious until three hours ago. No one took initiative until now. I am giving corporals orders. We have seven dead, 16 wounded, eight walking-wounded, the other 43 are still missing. Most probably burned up."

"Has anyone treated your wounds?" Cougar asked.

"There isn't time," Alish answered.

"You can't lead effectively if you're not treated."

Alish slumped against the wall of the overhang. Cougar dressed the gash on her forehead, but ignored her minor abrasions.

"Please, find my men," Alish said. "How many are with you?"

"I am the only medic."

"Please, tell your people not to harm my men. If you make any threats against us, we will die with weapons in our hands."

Cougar looked away from her face.

"You've killed already, haven't you?" she asked.

"One. He wasn't wounded."

"I suppose you were doing your job. Please don't do it again." Alish barely had energy to hold her rifle.

"And you're just doing yours."

"You know I will be court-martialed and hung for allowing you to help us," Alish said. "I haven't even saved very many men."

"We'll try to disappear before your help arrives. Don't shoot us in the back," Cougar said.

Alish frowned.

Cougar called up his teammates. Song stayed hidden. They didn't need to reveal his presence.

Cotton and Hard made orderly sweeps of the terrain with the assistance of two privates, but only recovered half of one body. Cougar treated the wounded already on the outcropping. He prayed for the rain to prevent reinforcements from coming until Clarice left.

Alish helped Cougar. Three more men died from their wounds and the cold. No one knew where Long or his staff were.

Private Archie leaned against a charred tree and lit a cigarette. The rain extinguished it. His face paled. His knees buckled.

Cougar caught him and laid him under the overhang.

"Poor guy, how long has he been working?" Cougar asked.

"He was the only one to respond to the crash. Everyone was too shocked or incapacitated. He hasn't rested at all," Alish replied.

Cougar pulled some jerky out of a tin and offered it to Alish.

"Don't you faint on me," he said.

"Why haven't you killed us yet?" she asked.

"ROEs. Wounded are wounded."

"Why are your ROEs so strict? I've heard legends about you."

"We make the legends so we can pull favors like this," Cougar said.

They made their rounds giving water to dying men.

"Why did you join Highroad Squad?" Alish asked.

"I wanted to be free to heal people. They're the only family I have. Why did you join the Cambrian army?" he asked her.

"To prove to the world that I was worth something. That my mother was right to believe in me," Alish fondled a pendant from her mother.

"That kind of thinking will turn you into a monster. I joined my squad instead of the army because the army offered what you want; ambition and unbridled power," Cougar said.

"I admire you," Alish said. "I live by your ROEs, you know. At least, the ones I have heard of."

"Then why were you aboard a murder machine?" Cougar turned with rage in his face.

"I was ordered to be on it. I am a propaganda officer. They wanted me to write about it."

"The weapon you carried destroyed my village. Everyone I loved died because of your ship," Cougar said.

"Then why are you helping me? If our men killed your family, then why?" Alish asked.

"I promised my wife I wouldn't become a monster like you. She was burnt just like your crew."

"I am sorry," Alish said.

"Then never do it again. If you were really brave, you would have destroyed the weapon!"

Alish's collected demeanor disappeared. Tears mingled with the raindrops.

"I will hang for allowing you to help us. I will tell my officers the truth of what happened because I will follow your ROEs and never lie."

A gunshot cracked in the distance. Cougar and Alish leveled their weapons at each other's chests.

"Where did that shot come from?" Alish demanded.

"I don't know. My friends wouldn't break their ROEs."

"Well, somebody broke orders!" Alish yelled.

Both soldiers lowered their weapons simultaneously.

"I don't want to kill you, but I will protect my men," both said in unison.

Cotton and Hard rushed up the outcropping with two scared privates behind them.

"What is going on?" Cotton shouted at Alish.

"I don't know. I didn't give any orders to shoot."

"Somebody did," Hard nursed a wound on his arm.

The rain stopped abruptly. In the moonlight, they saw 30 armed men in the burnt out clearing. The men at the campfire fell under their fire.

"What kind of trick is this?" Alish screamed. "Why are your people attacking?"

"They aren't our people," Cotton replied. "Song, get up here."

Song materialized in the field and rushed up the outcropping.

"You had a sharpshooter?" Alish asked.

"Precautions," Hard replied. "We don't trust you."

Song arrived, panting.

"Who are those guys?" Cotton asked.

"Wearing Cambrain uniforms. Started killing the wounded," Song said.

"Let me see," Alish raised her scoped rifle. "They're just killing them. Is this a false flag attack?"

"No, ma'am, it isn't," Song said. "Permission to engage."

"Granted," Cotton said.

Song dropped one of the enemies.

"They're headed for the wreckage, they'll kill that poor kid!" Cougar yelled.

Cotton explained about the kid while his men opened fire. Alish downed three enemies. An enemy tossed an incendiary grenade into the airship's cabin. Smoke billowed from inside. At least the kid didn't feel it.

Mortar fire erupted from the fringes of the forest. Shells burst all around them.

"Hard, find us an exit," Cotton yelled.

Hard scouted the back of the outcropping and found a ledge 20 meters down the back side of the stone. He borrowed Song's rope and tied it to a boulder.

"We can get down the back of the rock and get into a canyon. I don't know where it leads or if it is flooded, but we can't go forward."

"You can't leave my wounded behind," Private Archie said. "How will we get them out?"

"Do you have any more rope?" Hard yelled.

"No," Archie replied.

"Start moving them now," Cotton yelled. "We can't survive their mortar fire."

Cotton drew his pistol and lined up a shot carefully.

"Why are you using your pistol?" a private asked. "They are too far away."

"Rifle's out of ammo," Cotton said.

Soon, everyone ran out of ammo except Hard.

Alish placed her crosshairs on an enemy officer, then lowered her rifle.

"What was it?" Song asked.

"Long, Long was leading them."

"We need to get out now," Cotton said.

Enemies overran them. Long and his men held guns to everyone. Hard hid behind a boulder and twisted a makeshift tourniquet around his arm.

One of Long's guards pointed his shotgun at a wounded man and pulled the trigger.

Archie charged the guard, but Long's pistol silenced him.

"No witnesses," Long ordered. "But first I must interrogate these four." he waved his gun at Clarice and Alish.

Clarice stood with their hands on their heads. The last two wounded were killed along with the rest of Alish's men.

"Who are you?" his pistol leveled at Alish.

"Sergeant Desmond Alish, 457th Battalion, Propaganda."

"What are these men doing here?" Long demanded.

"They were helping the wounded. I admired you once, Long. I wanted to be you."

"You are ambitious, Sergeant. There is room for you in my new government."

"You stole the weapon, didn't you? You sabotaged the ship."

"Accidents happen," Long said.

"You will burn the capital, lead a coup, and establish yourself as leader. You are the traitor they warned us to look out for."

"I am loyal to the potential of Cambria," Long said. His granite expression never changed.

"I am loyal to the true Cambria. All you who hear me, do not betray our mothers. Do not join with this rat!" Alish ordered.

Colonel Long's fist broke her nose. While he was off balance from the punch, Cotton and Cougar seized him. Hard's last three bullets dropped the nearest guards.

Alish clutched her face with one hand and Long's pistol with the other. Song and Hard hustled her toward the rope. At the same time, Cotton and Cougar dragged Long behind a boulder.

Song stood at the top of the rope with Long's pistol. Enemies who tried to reach the Colonel met bullets from Song.

Long struggled free and rejoined his men. Cougar and Cotton dove for the rope. They burned their hands sliding down. Song emptied his last round into one of Long's men and tossed one of the knockout bottles. It shattered and disoriented his enemies.

As Song lowered himself, the Colonel tossed a grenade. It dropped straight toward Clarice. Song swatted it away, but lost his balance. He plummeted onto the ledge headfirst. The grenade exploded in the distance.

Cougar dragged Song's corpse beneath the cover of the canyon.

"We need to go now," Alish said. "Leave him."

"Can't leave him," Cotton said as he helped lift his dead friend. "We never leave anyone behind."

"I need to warn my people," Alish said as they hurried through the canyon.

"Let's get out alive first, then we'll help you warn them," Hard said.

"You promise?" she asked.

"Of course, Alish," Cotton said.
